package io.chino.java;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.common.ChinoApiConstants;
import io.chino.api.common.ChinoApiException;
import io.chino.api.common.Field;
import io.chino.api.document.Document;
import io.chino.api.document.DocumentsListFilter;
import io.chino.api.document.GetDocumentsBulkResponse;
import io.chino.api.document.GetDocumentsResponse;
import io.chino.api.schema.SchemaStructure;
import io.chino.api.search.DocumentsSearch;
import io.chino.api.search.ResultType;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.DeleteAll;
import io.chino.java.testutils.SchemaStructureSample;
import io.chino.java.testutils.TestConstants;
import kotlin.collections.ArrayDeque;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

import static io.chino.api.search.FilterOperator.EQUALS;
import static org.junit.Assert.*;

public class DocumentsTest extends ChinoBaseTest {

    private static ChinoAPI chino_admin;
    private static Documents test;

    private static String REPO_ID, SCHEMA_ID;

    @BeforeClass
    public static void beforeClass() throws IOException, ChinoApiException {
        ChinoBaseTest.runClass(DocumentsTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        test = ChinoBaseTest.init(chino_admin.documents);

        // create a repository and a Schema
        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.repositories.list().getRepositories().isEmpty(),
                chino_admin.repositories
        );
        REPO_ID = chino_admin.repositories.create("DocumentsTest"  + " [" + TestConstants.JAVA + "]")
                    .getRepositoryId();

        // fixme: if/when supporting creation of Schemas from a class, replace this with the code from UsersTest
        LinkedList<Field> fields = new LinkedList<>();
        fields.add(new Field("test_string", "string", true));

        SCHEMA_ID = chino_admin.schemas.create(
                REPO_ID,
                "this Schema is used to test Documents.",
                new SchemaStructure(fields)
        ).getSchemaId();
    }

    @AfterClass
    public static void afterClass() throws IOException, ChinoApiException {
        new DeleteAll().deleteAll(chino_admin);
        ChinoBaseTest.skipDelete();  // already deleted all the objects, it would be redundant
        ChinoBaseTest.afterClass();
    }

    @Test
    public void testToString_setContent() throws IOException, ChinoApiException {
        Document doc = newDoc("testToString_setContent");

        System.out.println(hr("toString()"));
        System.out.println(doc.toString());
        System.out.println(hr("END"));

        clear(doc);
    }

    @Test
    public void testList() throws IOException, ChinoApiException {
        Document doc = newDoc("testList");
        String variant = "1 param";

        try {
            List<Document> docList = test.list(SCHEMA_ID, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT, true).getDocuments();
            assertTrue(
                    docList.contains(doc)
            );

            variant = "2 params";
            List<Document> docsList = test.list(SCHEMA_ID, true).getDocuments();
            assertTrue(docsList.contains(doc));
            assertEquals( "Wrong Document found in list(). Maybe some old Document was not deleted?\n"
                            + docsList.get(0).getRepositoryId(),
                    doc.getContentAsHashMap(),
                    docsList.get(0).getContentAsHashMap()
            );

            variant = "3 params - expected 1 document";
            docsList = test.list(SCHEMA_ID, 0, 1, true).getDocuments();
            assertTrue(
                    docsList.contains(doc)
            );
            variant = "3 params - expected no documents";
            docsList = test.list(SCHEMA_ID, 1, 6, true).getDocuments();
            assertFalse(
                    docsList.contains(doc)
            );

            // 4 params variant is ignored bc already tested with the previous calls

        } catch (Exception e) {
            System.err.println("FAILED test variant with " + variant);
            throw e;
        } finally {
            clear(doc);
        }
    }

    @Test
    public void testList_withListFilters() throws Exception {
        // Get the current DateTime and then create a Document right after.
        // Expect the LIST methods to retrieve only 1 Document.

        // Store current timestamp. Use all supported formats (Date, LocalDateTime, String)
        Date nowDate = new Date();
        LocalDateTime nowLdt = nowDate.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
        String nowString = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").format(nowLdt);

        // Create Document
        Document doc = newDoc("testList_withListFilters");

        String variant = "";
        try {
            variant = "1 param - Date filter";
            DocumentsListFilter filter = DocumentsListFilter.filterBy()
                    .insertDate_greaterThan(nowDate)
                    .lastUpdate_greaterThan(nowDate)
                    .isActive(true);
            List<Document> docList = test.list(SCHEMA_ID, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());

            variant = "2 params - Date filter";
            docList = test.list(SCHEMA_ID, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());

            variant = "3 params - Date filter";
            docList = test.list(SCHEMA_ID, 0, 10, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());


            variant = "1 param - LocalDateTime filter";
            filter = DocumentsListFilter.filterBy()
                .insertDate_greaterThan(nowLdt)
                .lastUpdate_greaterThan(nowLdt)
                .isActive(true);
            docList = test.list(SCHEMA_ID, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());

            variant = "2 params - LocalDateTime filter";
            docList = test.list(SCHEMA_ID, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());

            variant = "3 params - LocalDateTime filter";
            docList = test.list(SCHEMA_ID, 0, 10, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());


            variant = "1 param - String filter";
            DocumentsListFilter.filterBy()
                .insertDate_greaterThan(nowString)
                .lastUpdate_greaterThan(nowString)
                .isActive(true);
            docList = test.list(SCHEMA_ID, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());

            variant = "2 params - String filter";
            docList = test.list(SCHEMA_ID, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());

            variant = "3 params - String filter";
            docList = test.list(SCHEMA_ID, 0, 10, true, filter).getDocuments();
            assertTrue(docList.contains(doc));
            assertEquals(1, docList.size());


            variant = "1 param - is_active=false";
            filter = DocumentsListFilter.filterBy()
                .insertDate_greaterThan(nowString)
                .isActive(false);
            docList = test.list(SCHEMA_ID, 0, ChinoApiConstants.QUERY_DEFAULT_LIMIT, true, filter).getDocuments();
            assertEquals(0, docList.size());

            variant = "2 params - is_active=false";
            docList = test.list(SCHEMA_ID, true, filter).getDocuments();
            assertEquals(0, docList.size());

            variant = "3 params - is_active=false";
            docList = test.list(SCHEMA_ID, 0, 10, true, filter).getDocuments();
            assertEquals(0, docList.size());

        } catch (Exception e) {
            System.err.println("FAILED test variant with " + variant);
            throw e;
        } finally {
            clear(doc);
        }
    }

    @Test
    public void testBulkGet() throws IOException, ChinoApiException {
        // Create 3 documents
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            Document document = newDoc("test"+i);
            documents.add(document);
        }

        List<String> documentIds = documents.stream().map(Document::getDocumentId).collect(Collectors.toList());
        GetDocumentsBulkResponse response = test.bulkGet(documentIds);

        assertNotNull(response);
        assertEquals(3, response.getCount().intValue());
        assertEquals(3, response.getTotalCount().intValue());
        assertEquals(0, response.getErrors().size());

        for (Map.Entry<String, Document> entry : response.getDocuments().entrySet()) {
            String documentId = entry.getKey();
            Document document = entry.getValue();

            assertTrue(documentIds.contains(documentId));
            assertEquals(documentId, document.getDocumentId());
        }

        // Delete all documents in the end
        for (Document document : documents) {
            clear(document);
        }

    }

    @Test
    public void testCreateRead_async() throws IOException, ChinoApiException {
        // create hashmap / read
        Document local = newDoc("testCreateReadUpdate_async");
        Document read_HashMap = test.read(local.getDocumentId());
        assertEquals( "Different content found after read() (created with HashMap)",
                local.getContentAsHashMap(),
                read_HashMap.getContentAsHashMap()
        );

        // create json string / read
        String localJson = new ObjectMapper().writeValueAsString(local.getContentAsHashMap());
        String id = test.create(SCHEMA_ID, localJson).getDocumentId();

        // fetch content - test read and method getContentAsHashMap()
        Document read_String = test.read(id);
        assertEquals( "Different content found after read() (created with JSON String)",
                // must have the same content as docStr
                local.getContentAsHashMap(),
                read_String.getContentAsHashMap()
        );
        assertEquals( "Different content between two read() Documents",
                // must have the same content as docStr
                read_HashMap.getContentAsHashMap(),
                read_String.getContentAsHashMap()
        );

        // read and map to class
        SchemaStructureSample docMapped = (SchemaStructureSample) test.read(
                local.getDocumentId(),
                SchemaStructureSample.class
        );
        assertEquals( "Mapped document doesn't contain the right values",
                local.getContentAsHashMap().get("test_string"),
                docMapped.test_string
        );

        clear(local);  // same ID of read_HashMap
        clear(read_String);
    }

    @Test
    public void testCreateRead_sync() throws IOException, ChinoApiException {
        // create hashmap / read
        HashMap<String, String> content = new HashMap<>();
        String methodName = "testCreateRead_sync";
        content.put("test_string", methodName);

        Document local = test.create(SCHEMA_ID, content, true);
        local.setContent(content);
        Document check = test.read(local.getDocumentId());
        assertEquals( "Different content found after read()  (created with HashMap)",
                local.getContentAsHashMap(),
                check.getContentAsHashMap()
        );

        // create json string / read
        String jsonContent = new ObjectMapper().writeValueAsString(local.getContentAsHashMap());
        DocumentsSearch search = (DocumentsSearch) chino_admin.search.documents(SCHEMA_ID)
                .with("test_string", EQUALS, methodName)
                .buildSearch();

        Document docStr = test.create(SCHEMA_ID, jsonContent, true);
        docStr.setContent(local.getContentAsHashMap());
        GetDocumentsResponse result = search.execute();
        assertTrue(
                result.getDocuments().contains(docStr)
        );

        clear(local);
        clear(docStr);
    }

    @Test
    public void testUpdateDelete_async() throws IOException, ChinoApiException {
        String name = "testUpdateDelete_async";
        String newName = "testUpdateDelete_async_updated";
        Document doc = newDoc(name);
        HashMap<String, Object> content = doc.getContentAsHashMap();
        content.put("test_string", newName);

        test.update(doc.getDocumentId(), content);
        Document check = test.read(doc.getDocumentId());
        assertNotEquals(
                check.getContentAsHashMap(),
                doc.getContentAsHashMap()
        );
        assertEquals( "read document and updated version do not match",
                content,
                check.getContentAsHashMap()
        );

        newName += "_String";
        content.put("test_string", newName);

        String jsonContent = new ObjectMapper().writeValueAsString(content);
        test.update(doc.getDocumentId(), jsonContent);
        check = test.read(doc.getDocumentId());
        assertNotEquals( "Document was not updated!",
                check.getContentAsHashMap(),
                doc.getContentAsHashMap()
        );
        assertEquals( "read document and updated version do not match",
                content,
                check.getContentAsHashMap()
        );

        test.delete(doc.getDocumentId(), true);
    }

    @Test
    public void testUpdateDelete_sync() throws IOException, ChinoApiException {
        String name = "testUpdateDelete_async";
        String newName = "testUpdateDelete_async_updated";
        Document doc = newDoc(name);
        HashMap<String, Object> content = doc.getContentAsHashMap();
        content.put("test_string", newName);

        test.update(doc.getDocumentId(), content);
        Document check = test.read(doc.getDocumentId());
        assertNotEquals(
                check.getContentAsHashMap(),
                doc.getContentAsHashMap()
        );
        assertEquals( "read document and updated version do not match",
                content,
                check.getContentAsHashMap()
        );

        newName += "_String";
        content.put("test_string", newName);


        DocumentsSearch search = (DocumentsSearch) chino_admin.search.documents(SCHEMA_ID).setResultType(ResultType.FULL_CONTENT)
                .with("test_string", EQUALS, newName)
                .buildSearch();
        doc = test.update(doc.getDocumentId(), content, true);
        doc.setContent(content);
        GetDocumentsResponse result = search.execute();
        assertTrue( "Unexpected content:\n" + result.getDocuments().get(0).toString() + "\n\nExpected:\n" + doc.toString(),
                result.getDocuments().contains(doc)
        );

        test.delete(doc.getDocumentId(), true);
    }

    @Test
    public void updatePartialSync_HashMap() throws IOException, ChinoApiException {
        String name = "updatePartial_HashMap";
        Document old = newDoc(name);

        int testCount = 0;
        for (boolean consistent : new boolean[] {true, false}) {
            String newName = name + "_" + (++testCount);

            HashMap<String, Object> updatedAttributes = new HashMap<>();
            updatedAttributes.put("test_string", newName);

            if (consistent)
                test.updatePartial(old.getDocumentId(), updatedAttributes, true);
            else
                test.updatePartial(old.getDocumentId(), updatedAttributes);
            Document updated = test.read(old.getDocumentId());

            assertEquals(old.getDocumentId(), updated.getDocumentId());
            assertEquals("document not updated!",
                    newName, updated.getContentAsHashMap().get("test_string"));

            old = updated;
        }
    }

    @Test
    public void updatePartial_String() throws IOException, ChinoApiException {
        String name = "updatePartial_String";
        Document old = newDoc(name);

        int testCount = 0;
        for (boolean consistent : new boolean[] {true, false}) {
            String newName = name + "_" + (++testCount);

            String updatedAttributes = "{" +
                    "  \"test_string\": \"" + newName + "\"" +
                    "}";

            if (consistent)
                test.updatePartial(old.getDocumentId(), updatedAttributes, true);
            else
                test.updatePartial(old.getDocumentId(), updatedAttributes);
            Document updated = test.read(old.getDocumentId());

            assertNotNull("document not updated!", updated);
            assertEquals("document not updated!",
                    newName, updated.getContentAsHashMap().get("test_string"));
            old = updated;
        }
    }

    @Test
    public void testActivate() throws Exception {
        Document doc1 = newDoc("testActivate1");
        Document doc2 = newDoc("testActivate2");
        Document doc3 = newDoc("testActivate3");
        Document doc4 = newDoc("testActivate4");
        String id1 = doc1.getDocumentId();
        String id2 = doc2.getDocumentId();
        String id3 = doc3.getDocumentId();
        String id4 = doc4.getDocumentId();
        // Set is_active = false
        test.delete(id1, false);
        test.delete(id2, false);
        test.delete(id3, false);
        test.delete(id4, false);
        assertFalse("Failed to set Document 1 inactive", test.read(id1).getIsActive());
        assertFalse("Failed to set Document 2 inactive", test.read(id2).getIsActive());
        assertFalse("Failed to set Document 3 inactive", test.read(id3).getIsActive());
        assertFalse("Failed to set Document 4 inactive", test.read(id4).getIsActive());

        // Set is_active = true
        HashMap<String, String> content1 = new HashMap<>();
        content1.put("test_string", "test_activation_updated");
        test.update(true, id1, content1);             // method 1: update(String, HashMap)
        Document read1 = test.read(id1);

        String content2 = "{" +
                    "\"test_string\": \"test_activation_updated\"" +
                "}";
        test.update(true, id2, content2);             // method 2: update(String, String)
        Document read2 = test.read(id2);

        HashMap<String, String> content3 = new HashMap<>();
        content3.put("test_string", "test_activation_updated_partial");
        test.updatePartial(true, id3, content3);      // method 3: updatePartial(String, HashMap)
        Document read3 = test.read(id3);

        String content4 = "{" +
                "\"test_string\": \"test_activation_updated_partial\"" +
                "}";
        test.updatePartial(true, id4, content4);      // method 4: updatePartial(String, String)
        Document read4 = test.read(id4);

        // Verify update
        assertTrue("Failed to activate Document 1", read1.getIsActive());
        assertNotEquals("Failed to update Document 1 after activation",
                doc1.getContentAsHashMap().get("test_string"),
                read1.getContentAsHashMap().get("test_string")
        );
        assertTrue("Failed to activate Document 2", read2.getIsActive());
        assertNotEquals("Failed to update Document 2 after activation",
                doc2.getContentAsHashMap().get("test_string"),
                read2.getContentAsHashMap().get("test_string")
        );
        assertTrue("Failed to activate Document 3", read3.getIsActive());
        assertNotEquals("Failed to update Document 3 after activation",
                doc3.getContentAsHashMap().get("test_string"),
                read3.getContentAsHashMap().get("test_string")
        );
        assertTrue("Failed to activate Document 4", read4.getIsActive());
        assertNotEquals("Failed to update Document 4 after activation",
                doc4.getContentAsHashMap().get("test_string"),
                read4.getContentAsHashMap().get("test_string")
        );

        // delete documents: even if there is an error, still try to delete all documents
        Exception[] errors = new Exception[4];
        try {
            test.delete(id1, true);
        } catch (IOException | ChinoApiException e1) {
            errors[0] = e1;
        }
        try {
            test.delete(id2, true);
        } catch (IOException | ChinoApiException e2) {
            errors[1] = e2;
        }
        try {
            test.delete(id3, true);
        } catch (IOException | ChinoApiException e3) {
            errors[2] = e3;
        }
        try {
            test.delete(id4, true);
        } catch (IOException | ChinoApiException e4) {
            errors[3] = e4;
        }

        if (errors[0] != null) throw errors[0];
        if (errors[1] != null) throw errors[1];
        if (errors[2] != null) throw errors[2];
        if (errors[3] != null) throw errors[3];
    }

    @Test
    public void testListFiltersParsing() throws Exception {
        Date date = new Date(0);
        LocalDateTime localDateTime = date.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
        String stringDate = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss").format(localDateTime);

        DocumentsListFilter filter = DocumentsListFilter.filterBy().insertDate_greaterThan(date);
        assertEquals("insert_date__gt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().insertDate_greaterThan(localDateTime);
        assertEquals("insert_date__gt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().insertDate_greaterThan(stringDate);
        assertEquals("insert_date__gt=1970-01-01T00:00:00", filter.toString());


        filter = DocumentsListFilter.filterBy().insertDate_lowerThan(date);
        assertEquals("insert_date__lt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().insertDate_lowerThan(localDateTime);
        assertEquals("insert_date__lt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().insertDate_lowerThan(stringDate);
        assertEquals("insert_date__lt=1970-01-01T00:00:00", filter.toString());


        filter = DocumentsListFilter.filterBy().lastUpdate_greaterThan(date);
        assertEquals("last_update__gt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().lastUpdate_greaterThan(localDateTime);
        assertEquals("last_update__gt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().lastUpdate_greaterThan(stringDate);
        assertEquals("last_update__gt=1970-01-01T00:00:00", filter.toString());


        filter = DocumentsListFilter.filterBy().lastUpdate_lowerThan(date);
        assertEquals("last_update__lt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().lastUpdate_lowerThan(localDateTime);
        assertEquals("last_update__lt=1970-01-01T00:00:00", filter.toString());

        filter = DocumentsListFilter.filterBy().lastUpdate_lowerThan(stringDate);
        assertEquals("last_update__lt=1970-01-01T00:00:00", filter.toString());


        filter = DocumentsListFilter.filterBy().isActive(true);
        assertEquals("is_active=true", filter.toString());

        filter = DocumentsListFilter.filterBy().isActive(false);
        assertEquals("is_active=false", filter.toString());

        // clear() method
        filter.clear();
        assertEquals("", filter.toString());

        // chained parameters
        filter = DocumentsListFilter.filterBy()
                .lastUpdate_lowerThan(localDateTime)
                .isActive(false);
        assertEquals("last_update__lt=1970-01-01T00:00:00&is_active=false", filter.toString());
    }

    private Document newDoc(SchemaStructureSample customContent, boolean consistent) throws IOException, ChinoApiException {
        String content = new ObjectMapper().writeValueAsString(customContent);

        Document newDoc;
        if (consistent)
            newDoc = test.create(SCHEMA_ID, content, consistent);
        else
            newDoc = test.create(SCHEMA_ID, content);

        newDoc.setContent(customContent.toHashMap());

        return newDoc;
    }

    private Document newDoc(String testName) throws IOException, ChinoApiException {
        SchemaStructureSample docContent = new SchemaStructureSample(testName);

        return newDoc(docContent, false);
    }

    private void clear(Document document) throws IOException, ChinoApiException {
        test.delete(document.getDocumentId(), true);

        System.out.println("Deleted Document.");
    }
}
