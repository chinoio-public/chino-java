package io.chino.java;

import io.chino.api.common.DumpFormat;
import io.chino.api.common.*;
import io.chino.api.schema.*;
import io.chino.api.search.FilterOperator;
import io.chino.api.search.SearchQueryBuilder;
import io.chino.api.search.SortRule;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.DeleteAll;
import io.chino.java.testutils.TestClassStructure;
import io.chino.java.testutils.TestConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class SchemasTest extends ChinoBaseTest {

    private static final String TEST_DUMP_FOLDER = "src/test/res/dump/schemas";
    private static ChinoAPI chino_admin;
    private static Schemas test;

    private static String REPO_ID;

    @BeforeClass
    public static void beforeClass() throws IOException, ChinoApiException {
        ChinoBaseTest.runClass(SchemasTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        test = ChinoBaseTest.init(chino_admin.schemas);

        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.repositories.list().getRepositories().isEmpty(),
                chino_admin.repositories
        ); // cleans repositories too

        REPO_ID = chino_admin.repositories.create("SchemasTest" + " [" + TestConstants.JAVA + "]")
                .getRepositoryId();
    }

    @AfterClass
    public static void afterClass() throws IOException, ChinoApiException {
        // delete all dump files
        Files.list(new File(TEST_DUMP_FOLDER).toPath()).forEach(path -> {
            try {
                Files.deleteIfExists(path);
            } catch (IOException e) {
                System.err.println("Failed to delete file after test: " + path.toAbsolutePath().toString());
            }
        });

        ChinoBaseTest.afterClass();
    }

    @Test
    public void test_CRUD() throws IOException, ChinoApiException {
        /* CREATE */
        String desc = "Schema created for SchemaTest in Java SDK";
        List<Field> fields = new LinkedList<>();
        fields.add(new Field ("test_update", "string"));
        SchemaStructure struc = new SchemaStructure(fields);
        SchemaRequest req = new SchemaRequest("SchemaTest", struc);

        Schema c_class = test.create(REPO_ID, desc, TestClassStructure.class);
        Schema c_struct = test.create(REPO_ID, desc, struc);
        Schema c_req = test.create(REPO_ID, req);

        /* READ */
        Schema r = test.read(c_class.getSchemaId());
        assertEquals("schemas.read() returned an unexpected object: " + r.toString(), c_class, r);

        /* UPDATE */
        String newDesc = "Updated Schema";

        List<Field> newFields = new LinkedList<>();
        newFields.add(new Field ("test_update", "string"));
        struc = new SchemaStructure(newFields);
        req = new SchemaRequest("SchemaTest", struc);

        // test Schema update with the same ID. Empty Schemas can be updated and fields can be removed.
        Schema u_class = test.update(c_class.getSchemaId(), newDesc, TestClassStructure.class);
        Schema u_struct = test.update(c_class.getSchemaId(), newDesc, struc);
        Schema u_req = test.update(c_class.getSchemaId(), req);

        assertNotEquals(c_class, u_class);
        assertNotEquals(c_struct, u_struct);
        assertNotEquals(c_req, u_req);

        /* DELETE */
        test.delete(c_class.getSchemaId(), true);
        try {
            test.read(c_class.getSchemaId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }

        test.delete(c_req.getSchemaId(), true);
        try {
            test.read(c_req.getSchemaId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }

        test.delete(c_struct.getSchemaId(), true);
        try {
            test.read(c_struct.getSchemaId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }
    }


    @Test
    public void test_list() throws IOException, ChinoApiException {
        Schema[] createdSchemas = new Schema[5];
        new DeleteAll().deleteAll(test);

        for (int i=0; i<5; i++) {
            createdSchemas[i] = makeSchema("test_list", i);
        }

        /* LIST (1 arg) */
        List<Schema> list = test.list(REPO_ID).getSchemas();
        int index = 0;
        for (Schema schema : createdSchemas) {
            assertTrue(
                    "schema" + ++index + " wasn't in the list.",
                    list.contains(schema)
            );
        }

        list.clear();

        /* LIST (3 args) */
        int offset = 0;
        int limit = 2;
        assertEquals( "Wrong list size (1)",
                limit,
                test.list(REPO_ID, offset, limit).getSchemas().size()
        );

        offset = createdSchemas.length - 1;
        limit = createdSchemas.length;
        assertEquals( "Wrong list size (2)",
                1,
                test.list(REPO_ID, offset, limit).getSchemas().size()
        );

        offset = 2;
        limit = createdSchemas.length;
        assertEquals( "Wrong list size (3)",
                limit - offset,
                test.list(REPO_ID, offset, limit).getSchemas().size()
        );

        /* LIST (filter by description, 1 arg) */
        String description = "_with_filter";
        Schema s = makeSchema("test_list" + description, 1);

        List<Schema> filtered = test.list(REPO_ID, description).getSchemas();

        assertEquals( "Wrong list size (1)", 1, filtered.size());
        assertTrue(
                String.format(
                        "Filter by description returned a wrong result: \"%s\" does not contain \"%s\"",
                        filtered.get(0).getDescription(), description
                ),
                filtered.get(0).getDescription().contains(description)
        );

        /* LIST (filter by description, 3 args) */
        // Just test that the method's code works, the logic is already tested in the previous steps.
        test.list(REPO_ID, description, 0, 1);
    }

    @Test
    public void test_activate() throws IOException, ChinoApiException {
        Schema schema = makeSchema("test_activation", 1);
        String id = schema.getSchemaId();
        // Set is_active = false
        test.delete(id, false);
        assertFalse("Failed to set inactive", test.read(id).getIsActive());
        // Set is_active = true
        // only need to test for one method - other methods
        test.update(true, id, "test_activation_updated", schema.getStructure());
        Schema control = test.read(id);
        // Verify update
        assertTrue("Failed to activate", control.getIsActive());
        assertNotEquals("Failed to update after activation",
                schema.getDescription(),
                control.getDescription()
        );

        test.delete(id, true);
    }

    @Test
    public void test_insensitiveFields() throws IOException, ChinoApiException {
        // test serialization
        List<Field> fields = new LinkedList<>();
        fields.add(new Field("case_insensitive", "string", true, true));
        fields.add(new Field("case_sensitive", "string", true, false));

        SchemaStructure structure = new SchemaStructure(fields);
        Schema s = test.create(REPO_ID, "test insensitive fields", structure);
        // test deserialization
        s = test.read(s.getSchemaId());

        SchemaStructure structureRead = s.getStructure();
        for (Field f : structureRead.getFields()) {
            boolean insensitive = f.getInsensitive();
            String name = f.getName();
            assertTrue(String.format("Field %s is%s insensitive", name, insensitive ? "" : " not"),
                    (name.equals("case_insensitive") && insensitive) ||
                            (name.equals("case_sensitive") && !insensitive)
            );
        }
    }

    @Test
    public void test_updateWithDefault() throws IOException, ChinoApiException {
        String desc = "Schema created for SchemaTest (with default) in Java SDK";

        /* CREATE */
        String fieldDefaulValue = "default";
        Field field = new Field ("test_update", "string").withDefault(fieldDefaulValue);
        SchemaStructure struc = new SchemaStructure().addField(field);

        Schema c = test.create(REPO_ID, desc, struc);

        /* READ */
        Schema r = test.read(c.getSchemaId());
        assertEquals("schemas.read() returned an unexpected object: " + r.toString(), c, r);
        assertEquals("Wrong amount of fields in the Schema after read()", 1,
                r.getStructure().getFields().size());
        Field f = r.getStructure().getFields().get(0);
        assertTrue(f.hasDefault());
        assertEquals(fieldDefaulValue, f.getDefault());

        /* UPDATE */
        String newFieldDefaultValue = fieldDefaulValue + " - updated";
        f.setDefault(newFieldDefaultValue);
        SchemaRequest req = new SchemaRequest(desc + "[update]", new SchemaStructure().addField(f));

        Schema u = test.update(r.getSchemaId(), req);
        assertEquals("Wrong amount of fields in the Schema after update()", 1,
                u.getStructure().getFields().size());
        Field f2 = u.getStructure().getFields().get(0);
        assertTrue(f2.hasDefault());
        assertEquals(newFieldDefaultValue, f2.getDefault());
        assertNotEquals(r.getDescription(), u.getDescription());
    }

    @Test
    public void test_datatypeConversion_Varargs() throws IOException, ChinoApiException {
        // Create Schema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("test_datatypeConversion_str", "string");
        fields.add(strField);
        Field intField = new Field ("test_datatypeConversion_int", "integer");
        fields.add(intField);

        SchemaStructure struc = new SchemaStructure(fields);
        SchemaRequest req = new SchemaRequest("Schema created for SchemaTest in Java SDK", struc);
        String schemaId = chino_admin.schemas.create(REPO_ID, req).getSchemaId();

        // start datatype conversion
        Field intToStrField = intField;
        intToStrField.setType("string");
        intToStrField.setDefault("NOT SET");

        // test with both fields, the strField should be ignored by the API because unchanged,
        // but should still be supported in the request body.
        DatatypeConversionStarted outcome = test.startDatatypeConversion(schemaId, strField, intToStrField);
        assertNotNull(outcome);
        assertTrue(outcome.getMessage().contains("/v1/schemas/convert/" + schemaId + "/status"));

        // check status
        DatatypeConversionStatus status = test.getDatatypeConversionStatus(schemaId);
        assertNotNull("'status' is null", status);
        assertNotNull("'Msgs' is null", status.getMsgs());
        assertNotNull("'Percentage' is null", status.getPercentage());
        assertNotNull("'Start' is null", status.getStart());
        assertNotNull("'Status' is null", status.getStatus());

        if (status.getStatus() == ConversionStatus.COMPLETED)
            assertNotNull("'End' is null", status.getEnd());
        else
            assertNull("'End' is not null", status.getEnd());
    }

    @Test
    public void test_datatypeConversion_Collection() throws IOException, ChinoApiException {
        // Create Schema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("test_datatypeConversion_str", "string");
        fields.add(strField);
        Field intField = new Field ("test_datatypeConversion_int", "integer");
        fields.add(intField);

        SchemaStructure struc = new SchemaStructure(fields);
        SchemaRequest req = new SchemaRequest("Schema created for SchemaTest in Java SDK", struc);
        String schemaId = chino_admin.schemas.create(REPO_ID, req).getSchemaId();

        // start datatype conversion
        Field intToStrField = intField;
        intToStrField.setType("string");
        intToStrField.setDefault("NOT SET");

        fields.clear();
        fields.add(strField);
        fields.add(intToStrField);

        // test with both fields, the strField should be ignored by the API because unchanged,
        // but should still be supported in the request body.
        DatatypeConversionStarted outcome = test.startDatatypeConversion(schemaId, fields);
        assertNotNull(outcome);
        assertTrue(outcome.getMessage().contains("/v1/schemas/convert/" + schemaId + "/status"));

        // check status
        DatatypeConversionStatus status = test.getDatatypeConversionStatus(schemaId);
        assertNotNull("'status' is null", status);
        assertNotNull("'Msgs' is null", status.getMsgs());
        assertNotNull("'Percentage' is null", status.getPercentage());
        assertNotNull("'Start' is null", status.getStart());
        assertNotNull("'Status' is null", status.getStatus());

        if (status.getStatus() == ConversionStatus.COMPLETED)
            assertNotNull("'End' is null", status.getEnd());
        else
            assertNull("'End' is not null", status.getEnd());
    }

    @Test
    public void test_datatypeConversion_HashMap() throws IOException, ChinoApiException, InterruptedException {
        // Create Schema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("test_datatypeConversion_str", "string");
        fields.add(strField);
        Field intField = new Field ("test_datatypeConversion_int", "integer");
        fields.add(intField);

        SchemaStructure struc = new SchemaStructure(fields);
        SchemaRequest req = new SchemaRequest("Schema created for SchemaTest in Java SDK", struc);
        String schemaId = chino_admin.schemas.create(REPO_ID, req).getSchemaId();

        // start datatype conversion
        HashMap<String, String> fieldsConversion = new HashMap();
        fieldsConversion.put(intField.getName(), "string");

        HashMap<String, Object> defaultValues = new HashMap();
        defaultValues.put(intField.getName(), "NOT_SET");

        // test with both fields, the strField should be ignored by the API because unchanged,
        // but should still be supported in the request body.
        DatatypeConversionStarted outcome = test.startDatatypeConversion(schemaId, fieldsConversion, defaultValues);
        assertNotNull(outcome);
        assertTrue(outcome.getMessage().contains("/v1/schemas/convert/" + schemaId + "/status"));

        // check status
        ConversionStatus status = ConversionStatus.QUEUED;
        while (status != ConversionStatus.COMPLETED) {
            DatatypeConversionStatus conversionProgress = test.getDatatypeConversionStatus(schemaId);
            status = conversionProgress.getStatus();
            assertNotNull("'status' is null", conversionProgress);
            assertNotNull("'Msgs' is null", conversionProgress.getMsgs());
            assertNotNull("'Percentage' is null", conversionProgress.getPercentage());
            assertNotNull("'Start' is null", conversionProgress.getStart());
            assertNotNull("'Status' is null", status);

            if (status == ConversionStatus.COMPLETED)
                assertNotNull("'End' is null", conversionProgress.getEnd());
            else
                assertNull("'End' is not null", conversionProgress.getEnd());

            // otherwise this check will be repeated every second
            Thread.sleep(1000);
        }
    }

    @Test
    @Deprecated
    public void test_schemaDump_deprecated() throws IOException, ChinoApiException, InterruptedException {
        // Create Schema
        Schema s = makeSchema("test_schemaDump", 1);
        // Create 4 documents
        HashMap<String, String> documentContent = new HashMap<>();
        documentContent.put("name", "John");
        chino_admin.documents.create(s.getSchemaId(), documentContent);
        documentContent.put("name", "Paul");
        chino_admin.documents.create(s.getSchemaId(), documentContent);
        documentContent.put("name", "Ringo");
        chino_admin.documents.create(s.getSchemaId(), documentContent);
        documentContent.put("name", "George");
        chino_admin.documents.create(s.getSchemaId(), documentContent);

        List<String> fieldNames = new LinkedList<>();
        s.getStructure().getFields().forEach(field -> fieldNames.add(field.getName()));

        for (io.chino.api.schema.DumpFormat format : io.chino.api.schema.DumpFormat.values()) {
            System.out.println("# # # " + format + " dump # # #");
            // Start dump (async operation)
            System.out.print("Starting Schema dump... ");
            String dumpId = test.startSchemaDump(s.getSchemaId(), fieldNames,
                    SearchQueryBuilder.not("name", FilterOperator.EQUALS, "Paul"),
                    Collections.singletonList(new SortRule("name", SortRule.Order.ASC)),
                    format);
            System.out.println("Done.");
            assertNotNull(dumpId);

            // Check progress of async task
            DumpStatus status = DumpStatus.QUEUED;
            while (status != DumpStatus.COMPLETED) {
                System.out.print("Checking status... ");
                SchemaDumpStatus dumpProgress = test.checkSchemaDumpStatus(dumpId);
                status = dumpProgress.getStatus();
                System.out.println(status);
                // If the task failed, stop the test
                assertNotEquals(ConversionStatus.FAILED_CONVERSION, status);
                assertNotEquals(ConversionStatus.FAILED_REINDEX, status);
                // otherwise this check will be repeated every second
                Thread.sleep(1000);
            }

            // Download Schema dump
            System.out.print("Downloading dump... ");
            File dump = test.getSchemaDump(dumpId, TEST_DUMP_FOLDER, s.getSchemaId() + "-dump." + format);
            System.out.println("Done.");
            // check File was created
            System.out.print("Checking dump file... ");
            assertTrue(dump.exists());
            System.out.println("Found: " + dump.getAbsolutePath());
            // check content
            System.out.print("Checking file content... ");
            List<String> lines = Files.readAllLines(dump.toPath());
            StringBuilder content = new StringBuilder();
            lines.forEach(line -> content.append(line).append("\n"));
            assertTrue(content.toString(), content.toString().contains("John"));
            assertTrue(content.toString(), content.toString().contains("Ringo"));
            assertTrue(content.toString(), content.toString().contains("George"));
            // This Beatle is excluded by the "query" argument
            assertFalse(content.toString().contains("Paul"));
            System.out.println("Done.");
        }
    }

    @Test
    public void test_schemaDump() throws IOException, ChinoApiException, InterruptedException {
        // Create Schema
        Schema s = makeSchema("test_schemaDump", 1);
        // Create 4 documents
        HashMap<String, String> documentContent = new HashMap<>();
        documentContent.put("name", "John");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);
        documentContent.put("name", "Paul");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);
        documentContent.put("name", "Ringo");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);
        documentContent.put("name", "George");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);

        List<String> fieldNames = new LinkedList<>();
        s.getStructure().getFields().forEach(field -> fieldNames.add(field.getName()));

        for (DumpFormat format : DumpFormat.values()) {
            System.out.println("# # # " + format + " dump # # #");
            // Start dump (async operation)
            System.out.print("Starting Schema dump... ");
            String dumpId = test.startSchemaDump(s.getSchemaId(), fieldNames,
                    SearchQueryBuilder.not("name", FilterOperator.EQUALS, "Paul"),
                    Collections.singletonList(new SortRule("name", SortRule.Order.ASC)),
                    format);
            System.out.println("Done.");
            assertNotNull(dumpId);

            // Check progress of async task
            DumpStatus status = DumpStatus.QUEUED;
            while (status != DumpStatus.COMPLETED) {
                System.out.print("Checking status... ");
                SchemaDumpStatus dumpProgress = test.checkSchemaDumpStatus(dumpId);
                status = dumpProgress.getStatus();
                System.out.println(status);
                // If the task failed, stop the test
                assertNotEquals(ConversionStatus.FAILED_CONVERSION, status);
                assertNotEquals(ConversionStatus.FAILED_REINDEX, status);
                // otherwise this check will be repeated every second
                Thread.sleep(1000);
            }

            // Download Schema dump
            System.out.print("Downloading dump... ");
            File dump = test.getSchemaDump(dumpId, TEST_DUMP_FOLDER, s.getSchemaId() + "-dump." + format);
            System.out.println("Done.");
            // check File was created
            System.out.print("Checking dump file... ");
            assertTrue(dump.exists());
            System.out.println("Found: " + dump.getAbsolutePath());
            // check content
            System.out.print("Checking file content... ");
            List<String> lines = Files.readAllLines(dump.toPath());
            StringBuilder content = new StringBuilder();
            lines.forEach(line -> content.append(line).append("\n"));
            assertTrue("'John' not found in:\n" + content.toString(), content.toString().contains("John"));
            assertTrue("'Ringo' not found in:\n" + content.toString(), content.toString().contains("Ringo"));
            assertTrue("'George' not found in:\n" + content.toString(), content.toString().contains("George"));
            // This Beatle is excluded by the "query" argument
            assertFalse(content.toString().contains("Paul"));
            System.out.println("Done.");
        }
    }

    @Test
    public void test_deleteAllContent() throws IOException, ChinoApiException, InterruptedException {
        // Create Schema
        Schema s = makeSchema("test_schemaDump", 1);
        // Create 4 documents
        HashMap<String, String> documentContent = new HashMap<>();
        documentContent.put("name", "John");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);
        documentContent.put("name", "Paul");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);
        documentContent.put("name", "Ringo");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);
        documentContent.put("name", "George");
        chino_admin.documents.create(s.getSchemaId(), documentContent, true);

        // Attempt delete Schema WITHOUT flag `all_content`
        try {
            test.delete(s.getSchemaId(), true);
            fail("Schema.delete() worked on non-empty Schema without all_content flag");
        } catch (ChinoApiException e) {
            assertEquals("400", e.getCode());
        }
        // Attempt delete Schema with the flag
        test.delete(s.getSchemaId(), true, true);
    }

    private synchronized Schema makeSchema(String testName, int instanceNumber) throws IOException, ChinoApiException {
        String desc = testName + "_" + instanceNumber;

        Schema res = test.create(REPO_ID, desc, TestClassStructure.class);
        try {
            wait(3000);
        } catch (InterruptedException ignored) {}
        return res;
    }

}