package io.chino.java;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.common.ChinoApiException;
import io.chino.api.group.Group;
import io.chino.api.user.GetUsersResponse;
import io.chino.api.user.User;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.TestClassStructure;
import io.chino.java.testutils.TestConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class GroupsTest extends ChinoBaseTest {

    private static ChinoAPI chino_admin;
    private static Groups test;

    private static String USER_SCHEMA_ID;

    @BeforeClass
    public static void beforeClass() throws IOException, ChinoApiException {
        ChinoBaseTest.runClass(GroupsTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        test = ChinoBaseTest.init(chino_admin.groups);

        // cleanup groups
        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.groups.list().getGroups().isEmpty(),
                chino_admin.groups
        );

        // create a UserSchema
        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.userSchemas.list().getUserSchemas().isEmpty(),
                chino_admin.userSchemas
        );

        USER_SCHEMA_ID = chino_admin.userSchemas.create("GroupsTest" + " [" + TestConstants.JAVA + "]", TestClassStructure.class)
                            .getUserSchemaId();
    }

    @Test
    public void test_CRUD() throws IOException, ChinoApiException {
        /* CREATE */
        String groupName = "java GroupsTest" + "[" + TestConstants.JAVA + "]";
        //System.out.println("Group name: " + groupName + " (length: " + groupName.length() + ")");

        Group c = test.create(groupName, new HashMap());
        assertNotNull("Group was not created! (null reference)", c);

        /* READ */
        Group r = test.read(c.getGroupId());
        assertEquals("Read object is different from original", c, r);

        /* UPDATE */
        HashMap<String, Object> attrs = new HashMap<>();
        attrs.put("attr", "");
        Group u = test.update(c.getGroupId(), "UPDATED" + " [" + TestConstants.JAVA + "]", attrs);

        assertNotEquals("Object was not updated", c, u);
        assertEquals("Update failed", "UPDATED" + " [" + TestConstants.JAVA + "]", u.getGroupName());
        HashMap<String, Object> control = new ObjectMapper().convertValue(u.getAttributes(),
                new TypeReference<HashMap<String, Object>>() {}
        );
        assertFalse("Group attributes weren't updated", control.isEmpty());
        assertTrue(control.containsKey("attr"));

        /* DELETE */
        test.delete(u.getGroupId(), true);

        try {
            test.read(c.getGroupId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }
    }

    @Test
    public void test_list() throws IOException, ChinoApiException {
        Group[] groups = {
                test.create("test_list_group1" + " [" + TestConstants.JAVA + "]", new HashMap()),
                test.create("test_list_group2" + " [" + TestConstants.JAVA + "]", new HashMap()),
                test.create("test_list_group3" + " [" + TestConstants.JAVA + "]", new HashMap()),
                test.create("test_list_group4" + " [" + TestConstants.JAVA + "]", new HashMap()),
                test.create("test_list_group5" + " [" + TestConstants.JAVA + "]", new HashMap())
        };
        /* LIST (no args) */
        assertEquals( "Missing groups in list",
                groups.length,
                test.list().getGroups().size()
        );

        /* LIST (2 args) */
        int offset = 0;
        int limit = 2;
        assertEquals("Wrong list size (1)",
                limit,
                test.list(offset, limit).getGroups().size()
        );

        offset = groups.length - 1;
        limit = groups.length;
        assertEquals( "Wrong list size (2)",
                1,
                test.list(offset, limit).getGroups().size()
        );

        offset = 2;
        limit = groups.length;
        assertEquals( "Wrong list size (3)",
                limit - offset,
                test.list(offset, limit).getGroups().size()
        );

        /* LIST (filter by description, 1 arg) */
        String groupName = "_with_filter";
        Group g = test.create("test_list_" + groupName, new HashMap());

        List<Group> filtered = test.list(groupName).getGroups();

        assertEquals( "Wrong list size (1)", 1, filtered.size());
        assertTrue(
                String.format(
                        "Filter by group name returned a wrong result: \"%s\" does not contain \"%s\"",
                        filtered.get(0).getGroupName(), groupName
                ),
                filtered.get(0).getGroupName().contains(groupName)
        );

        /* LIST (filter by description, 3 args) */
        // Just test that the method's code works, the logic is already tested in the previous steps.
        test.list(groupName, 0, 1);
    }

    @Test
    public void test_users() throws IOException, ChinoApiException {
        User[] users = {
                makeUser("test_users_usr1"),
                makeUser("test_users_usr2"),
                makeUser("test_users_usr3"),
                makeUser("test_users_usr4"),
                makeUser("test_users_usr5")
        };
        Group group = test.create("test_users" + " [" + TestConstants.JAVA + "]", new HashMap());

        /* ADD */
        for (User u : users) {
            test.addUserToGroup(u.getUserId(), group.getGroupId());
            assertUserIsIn(u, group);
        }

        /* LIST USERS */
        int offset = 1, limit = 2;
        GetUsersResponse response = test.listUsers(group.getGroupId(), offset, limit);
        assertEquals("Wrong count returned by listUsers", limit, (int) response.getCount());
        assertEquals("Wrong count returned by listUsers", limit, response.getUsers().size());

        GetUsersResponse responseAll = test.listUsers(group.getGroupId());
        List<String> userIds = new LinkedList<>();
        for (User userInGroup : responseAll.getUsers()) {
            userIds.add(userInGroup.getUserId());
        }
        for (User u : users) {
            assertTrue("Missing User " + u.getUserId() + " in list of users for Group " + group.getGroupId(),
                    userIds.contains(u.getUserId()));
        }


        /* REMOVE */
        for (User u : users) {
            test.removeUserFromGroup(u.getUserId(), group.getGroupId());
            assertUserIsNotIn(u, group);
        }

        // Skipping these tests because they don't work at the moment
        // TODO uncomment all as soon as the addition of UserSchemas to a Group works correctly
//        /* ADD USER SCHEMA */
//        test.addUserSchemaToGroup(USER_SCHEMA_ID, group.getGroupId());
//        assertUserIsIn(users[0], group);
//        assertUserIsIn(
//                makeUser("test_user_new1"),
//                group
//        );
//
//        /* REMOVE USER SCHEMA */
//        test.removeUserSchemaFromGroup(USER_SCHEMA_ID, group.getGroupId());
//        assertUserIsNotIn(users[0], group);
//        assertUserIsNotIn(
//                makeUser("test_user_new2"),
//                group
//        );
    }

    @Test
    public void testActivate() throws IOException, ChinoApiException {
        Group group = test.create("testActivate", new HashMap());
        String id = group.getGroupId();
        // Set is_active = false
        test.delete(id, false);
        assertFalse("Failed to set inactive", test.read(id).getIsActive());
        // Set is_active = true
        HashMap<String, Object> attributes = new HashMap<>();
        attributes.put("updated", true);
        test.update(true, id, "test_activation_updated", attributes);
        Group control = test.read(id);
        // Verify update
        assertTrue("Failed to activate", control.getIsActive());
        assertNotEquals("Failed to update after activation",
                group.getGroupName(),
                control.getGroupName()
        );
        assertTrue("Failed to add attributes after activation",
                control.getAttributes().has("updated")
        );

        test.delete(id, true);
    }

    @Test
    public void testAttributes() throws IOException, ChinoApiException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("YYYY-MM-dd");

        HashMap<String, Object> attributes = new HashMap<String, Object>();
        attributes.put("test_string", "test");
        attributes.put("test_integer", 123);
        attributes.put("test_float", 1.23);
        attributes.put("test_boolean", true);
        attributes.put("test_date", dateFormat.format(new Date()));
        attributes.put("test_string_array", new String[] {"s1", "s2"});
        attributes.put("test_string_array_list", Arrays.asList("s1", "s2"));
        attributes.put("test_integer_array", new int[] {1, 2, 3});
        attributes.put("test_integer_array_list", Arrays.asList(1, 2, 3));
        attributes.put("test_boolean_array", new Boolean[] {true, true, false});
        attributes.put("test_boolean_array_list", Arrays.asList(true, true, false));
        HashMap<String, Object> nestedJson = new HashMap<>();
        nestedJson.put("success", true);
        attributes.put("test_json", nestedJson);

        String groupName = "java GroupsTest.testAttributes" + "[" + TestConstants.JAVA + "]";
        String groupId = test.create(groupName, attributes).getGroupId();

        // Read group and check that all attributes are present
        Group g = test.read(groupId);
        HashMap expected = attributes;
        HashMap actual = g.getAttributesAsHashMap();
        attributes.forEach((key, expectedValue) -> {
            assertTrue("Missing key '" + key + "'' in Group attributes",
                actual.containsKey(key));
        });

        // Read attrs one by one and check datatype
        assertEquals("Wrong attr: test_string.",
            expected.get("test_string"), actual.get("test_string"));
        assertEquals("Wrong attr: test_integer.",
            expected.get("test_integer"), actual.get("test_integer"));
        assertEquals("Wrong attr: test_float.",
            expected.get("test_float"), actual.get("test_float"));
        assertEquals("Wrong attr: test_boolean.",
            expected.get("test_boolean"), actual.get("test_boolean"));
        assertEquals("Wrong attr: test_date.",
            expected.get("test_date"), actual.get("test_date"));
        // expect a List instead of a String array
        assertEquals("Wrong attr: test_string_array.",
            Arrays.asList("s1", "s2"), actual.get("test_string_array")
        );
        assertEquals("Wrong attr: test_string_array_list.",
            expected.get("test_string_array_list"), actual.get("test_string_array_list"));
        // expect a List instead of a int array
        assertEquals("Wrong attr: test_integer_array.",
            Arrays.asList(1, 2, 3), actual.get("test_integer_array")
        );
        assertEquals("Wrong attr: test_integer_array_list.",
            expected.get("test_integer_array_list"), actual.get("test_integer_array_list"));
        // expect a List instead of a boolean array
        assertEquals("Wrong attr: test_boolean_array.",
            Arrays.asList(true, true, false), actual.get("test_boolean_array")
        );
        assertEquals("Wrong attr: test_boolean_array_list.",
            expected.get("test_boolean_array_list"), actual.get("test_boolean_array_list"));
        // Test that nested JSON is converted to a Map
        assertEquals("Wrong attr: test_json.", nestedJson, actual.get("test_json"));
    }

    private static void assertUserIsIn(User user, Group group) throws IOException, ChinoApiException {
        User check = chino_admin.users.read(user.getUserId());
        List groups = check.getGroups();

        assertTrue(
            "User " + user.getUsername() + " is not in group " + group.getGroupName(),
            groups.contains(group.getGroupId())
        );
    }

    private static void assertUserIsNotIn(User user, Group group) throws IOException, ChinoApiException {
        User check = chino_admin.users.read(user.getUserId());
        List groups = check.getGroups();

        assertFalse(
            "User " + user.getUsername() + " is still in group " + group.getGroupName(),
            groups.contains(group.getGroupId())
        );
    }

    private User makeUser(String name) throws IOException, ChinoApiException {
        return chino_admin.users.create(name, "1234567890", new TestClassStructure(name).toMap(), USER_SCHEMA_ID);
    }

    @AfterClass
    public static void afterClass() throws IOException, ChinoApiException {
        ChinoBaseTest.afterClass();
    }
}
