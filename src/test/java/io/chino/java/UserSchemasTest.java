package io.chino.java;

import io.chino.api.common.*;
import io.chino.api.schema.DumpStatus;
import io.chino.api.schema.SchemaDumpStatus;
import io.chino.api.search.FilterOperator;
import io.chino.api.search.SearchQueryBuilder;
import io.chino.api.search.SortRule;
import io.chino.api.userschema.UserSchema;
import io.chino.api.userschema.UserSchemaRequest;
import io.chino.api.userschema.UserSchemaStructure;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.DeleteAll;
import io.chino.java.testutils.TestClassStructure;
import io.chino.java.testutils.TestConstants;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class UserSchemasTest extends ChinoBaseTest {

    private static final String TEST_DUMP_FOLDER = "src/test/res/dump/user_schemas";
    private static ChinoAPI chino_admin;
    private static UserSchemas test;

    @BeforeClass
    public static void beforeClass() throws IOException, ChinoApiException {
        ChinoBaseTest.runClass(UserSchemasTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        test = ChinoBaseTest.init(chino_admin.userSchemas);

        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.userSchemas.list().getUserSchemas().isEmpty(),
                chino_admin.userSchemas
        );
    }

    @AfterClass
    public static void afterClass() throws IOException, ChinoApiException {
        // delete all dump files
        Files.list(new File(TEST_DUMP_FOLDER).toPath()).forEach(path -> {
            try {
                Files.deleteIfExists(path);
            } catch (IOException e) {
                System.err.println("Failed to delete file after test: " + path.toAbsolutePath().toString());
            }
        });

        ChinoBaseTest.afterClass();
    }

    @Test
    public void test_CRUD_Class() throws IOException, ChinoApiException {
        /* CREATE */
        UserSchema c = test.create("UserSchema created for testing purposes"  + " [" + TestConstants.JAVA + "]",
                TestClassStructure.class
        );

        /* READ */
        UserSchema r = test.read(c.getUserSchemaId());
        assertEquals("userSchemas.read() returned an unexpected object: " + r.toString(), c, r);

        /* UPDATE */
        String newDesc = "Updated description for UserSchema"  + " [" + TestConstants.JAVA + "]";

        UserSchema u = test.update(c.getUserSchemaId(), newDesc, TestClassStructure.class);
        assertEquals("Updated userschema has different ID!", r.getUserSchemaId(), u.getUserSchemaId());
        assertNotEquals(r.getDescription(), u.getDescription());
        assertEquals(r.getStructure(), u.getStructure());

        /* DELETE */
        test.delete(c.getUserSchemaId(), true);
        try {
            test.read(c.getUserSchemaId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }
    }

    @Test
    public void test_CUD_UserSchemaRequest() throws IOException, ChinoApiException {
        LinkedList<Field> fields = new LinkedList<>();
        fields.add(new Field("testMethod", "string", true));
        UserSchemaStructure struct = new UserSchemaStructure(fields);
        UserSchemaRequest req = new UserSchemaRequest(
                "UserSchema created for testing purposes"  + " [" + TestConstants.JAVA + "]",
                struct
        );

        /* CREATE */
        UserSchema c = test.create(req);

        /* UPDATE */
        String newDesc = "Updated description for UserSchema"  + " [" + TestConstants.JAVA + "]";
        req.setDescription(newDesc);

        UserSchema u = test.update(c.getUserSchemaId(), req);
        assertEquals("Updated userschema has different ID!", c.getUserSchemaId(), u.getUserSchemaId());
        assertNotEquals(c.getDescription(), u.getDescription());
        assertEquals(c.getStructure(), u.getStructure());

        /* DELETE */
        test.delete(c.getUserSchemaId(), true);
        try {
            test.read(c.getUserSchemaId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }
    }

    @Test
    public void test_CUD_UserSchemaStructure() throws IOException, ChinoApiException {
        LinkedList<Field> fields = new LinkedList<>();
        fields.add(new Field("testMethod", "string", true));

        /* CREATE */
        UserSchema c = test.create(
                "UserSchema created for testing purposes"  + " [" + TestConstants.JAVA + "]",
                new UserSchemaStructure(fields)
        );

        /* UPDATE */
        String newDesc = "Updated description for UserSchema"  + " [" + TestConstants.JAVA + "]";

        UserSchema u = test.update(c.getUserSchemaId(), newDesc, new UserSchemaStructure(fields));
        assertEquals("Updated userschema has different ID!", c.getUserSchemaId(), u.getUserSchemaId());
        assertNotEquals(c.getDescription(), u.getDescription());
        assertEquals(c.getStructure(), u.getStructure());

        /* DELETE */
        test.delete(c.getUserSchemaId(), true);
        try {
            test.read(c.getUserSchemaId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }
    }

    @Test
    public void test_list() throws IOException, ChinoApiException {
        UserSchema[] userSchemas = new UserSchema[5];
        new DeleteAll().deleteAll(test);

        synchronized (this) {
            for (int i=0; i<5; i++) {
                userSchemas[i] = test.create("UserSchema_list_test_" + (i+1)  + " [" + TestConstants.JAVA + "]", TestClassStructure.class);
                try {
                    wait(3000);
                } catch (InterruptedException ignored) {
                }
            }
        }

        /* LIST (0 args) */
        List<UserSchema> fetchedList = test.list().getUserSchemas();
        assertEquals("Couldn't fetch all the UserSchemas!", userSchemas.length, fetchedList.size());
        for (UserSchema us : userSchemas) {
            assertTrue(us.getDescription() + " is not in the fetched list!", fetchedList.contains(us));
        }

        /* LIST (2 args) */
        int limit = 3;
        int offset = 0;
        fetchedList = test.list(offset, limit).getUserSchemas();
        assertEquals("Couldn't fetch all the UserSchemas!", limit, fetchedList.size());

        offset = 4;
        limit = 10;
        fetchedList = test.list(offset, limit).getUserSchemas();
        assertEquals("Couldn't fetch all the UserSchemas!", userSchemas.length - offset, fetchedList.size());

        /* LIST (filter by description, 1 arg) */
        String description = "_with_filter";
        UserSchema s = test.create("UserSchema_list_test" + description, TestClassStructure.class);

        List<UserSchema> filtered = test.list(description).getUserSchemas();

        assertEquals( "Wrong list size (1)", 1, filtered.size());
        assertTrue(
                String.format(
                        "Filter by description returned a wrong result: \"%s\" does not contain \"%s\"",
                        filtered.get(0).getDescription(), description
                ),
                filtered.get(0).getDescription().contains(description)
        );

        /* LIST (filter by description, 3 args) */
        // Just test that the method's code works, the logic is already tested in the previous steps.
        test.list(description, 0, 1);
    }

    @Test
    public void test_activate() throws IOException, ChinoApiException {
        UserSchema us0 = test.create("test_activation0", TestClassStructure.class);
        UserSchema us1 = test.create("test_activation1", TestClassStructure.class);
        int iteration = -1;
        for (UserSchema us : new UserSchema[] {us0, us1}) {
            String id = us.getUserSchemaId();
            // Set is_active = false
            test.delete(id, false);
            assertFalse("Failed to set inactive", test.read(id).getIsActive());
            // Set is_active = true with different methods
            doActivation(++iteration, id, us.getStructure());
            UserSchema control = test.read(id);
            // Verify update
            assertTrue("Failed to activate", control.getIsActive());
            assertNotEquals("Failed to update string attribute after activation",
                    us.getDescription(),
                    control.getDescription()
            );
            assertTrue("Failed to update integer attribute after activation",
                    control.getDescription().endsWith(":" + iteration)
            );

            test.delete(id, true);
        }
    }

    @Test
    public void test_insensitiveFields() throws IOException, ChinoApiException {
        // test serialization
        List<Field> fields = new LinkedList<>();
        fields.add(new Field("case_insensitive", "string", true, true));
        fields.add(new Field("case_sensitive", "string", true, false));

        UserSchemaStructure structure = new UserSchemaStructure(fields);
        UserSchema s = test.create("test insensitive fields", structure);
        // test deserialization
        s = test.read(s.getUserSchemaId());

        UserSchemaStructure structureRead = s.getStructure();
        for (Field f : structureRead.getFields()) {
            boolean insensitive = f.getInsensitive();
            String name = f.getName();
            assertTrue(String.format("Field %s is%s insensitive", name, insensitive ? "" : " not"),
                    (name.equals("case_insensitive") && insensitive) ||
                    (name.equals("case_sensitive") && !insensitive)
            );
        }
    }

    @Test
    public void test_updateWithDefault() throws IOException, ChinoApiException {
        String desc = "UserSchema created for SchemaTest (with default) in Java SDK";

        /* CREATE */
        String fieldDefaulValue = "default";
        Field field = new Field ("test_update", "string").withDefault(fieldDefaulValue);
        UserSchemaStructure struc = new UserSchemaStructure().addField(field);
        UserSchemaRequest req = new UserSchemaRequest("SchemaTest", struc);

        UserSchema c = test.create(desc, struc);

        /* READ */
        UserSchema r = test.read(c.getUserSchemaId());
        assertEquals("schemas.read() returned an unexpected object: " + r.toString(), c, r);
        assertEquals("Wrong amount of fields in the UserSchema after read()", 1,
                r.getStructure().getFields().size());
        Field f = r.getStructure().getFields().get(0);
        assertTrue(f.hasDefault());
        assertEquals(fieldDefaulValue, f.getDefault());

        /* UPDATE */
        String newFieldDefaultValue = fieldDefaulValue + " - updated";
        f.setDefault(newFieldDefaultValue);
        req = new UserSchemaRequest(desc + "[update]", new UserSchemaStructure().addField(f));

        UserSchema u = test.update(r.getUserSchemaId(), req);
        assertEquals("Wrong amount of fields in the UserSchema after update()", 1,
                u.getStructure().getFields().size());
        Field f2 = u.getStructure().getFields().get(0);
        assertTrue(f2.hasDefault());
        assertEquals(newFieldDefaultValue, f2.getDefault());
        assertNotEquals(r.getDescription(), u.getDescription());
    }

    @Test
    public void test_datatypeConversion_Varargs() throws IOException, ChinoApiException {
        // Create UserSchema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("test_datatypeConversion_str", "string");
        fields.add(strField);
        Field intField = new Field ("test_datatypeConversion_int", "integer");
        fields.add(intField);

        UserSchemaStructure struc = new UserSchemaStructure(fields);
        UserSchemaRequest req = new UserSchemaRequest("UserSchema created for SchemaTest in Java SDK", struc);
        String userSchemaId = chino_admin.userSchemas.create(req).getUserSchemaId();

        // start datatype conversion
        Field intToStrField = intField;
        intToStrField.setType("string");
        intToStrField.setDefault("NOT SET");

        // test with both fields, the strField should be ignored by the API because unchanged,
        // but should still be supported in the request body.
        DatatypeConversionStarted outcome = test.startDatatypeConversion(userSchemaId, strField, intToStrField);
        assertNotNull(outcome);
        assertTrue(outcome.getMessage().contains("/v1/user_schemas/convert/" + userSchemaId + "/status"));

        // check status
        DatatypeConversionStatus status = test.getDatatypeConversionStatus(userSchemaId);
        assertNotNull("'status' is null", status);
        assertNotNull("'Msgs' is null", status.getMsgs());
        assertNotNull("'Percentage' is null", status.getPercentage());
        assertNotNull("'Start' is null", status.getStart());
        assertNotNull("'Status' is null", status.getStatus());

        if (status.getStatus() == ConversionStatus.COMPLETED)
            assertNotNull("'End' is null", status.getEnd());
        else
            assertNull("'End' is not null", status.getEnd());
    }

    @Test
    public void test_datatypeConversion_Collection() throws IOException, ChinoApiException {
        // Create UserSchema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("test_datatypeConversion_str", "string");
        fields.add(strField);
        Field intField = new Field ("test_datatypeConversion_int", "integer");
        fields.add(intField);

        UserSchemaStructure struc = new UserSchemaStructure(fields);
        UserSchemaRequest req = new UserSchemaRequest("UserSchema created for SchemaTest in Java SDK", struc);
        String userSchemaId = chino_admin.userSchemas.create(req).getUserSchemaId();

        // start datatype conversion
        Field intToStrField = intField;
        intToStrField.setType("string");
        intToStrField.setDefault("NOT SET");

        fields.clear();
        fields.add(strField);
        fields.add(intToStrField);

        // test with both fields, the strField should be ignored by the API because unchanged,
        // but should still be supported in the request body.
        DatatypeConversionStarted outcome = test.startDatatypeConversion(userSchemaId, fields);
        assertNotNull(outcome);
        assertTrue(outcome.getMessage().contains("/v1/user_schemas/convert/" + userSchemaId + "/status"));

        // check status
        DatatypeConversionStatus status = test.getDatatypeConversionStatus(userSchemaId);
        assertNotNull("'status' is null", status);
        assertNotNull("'Msgs' is null", status.getMsgs());
        assertNotNull("'Percentage' is null", status.getPercentage());
        assertNotNull("'Start' is null", status.getStart());
        assertNotNull("'Status' is null", status.getStatus());

        if (status.getStatus() == ConversionStatus.COMPLETED)
            assertNotNull("'End' is null", status.getEnd());
        else
            assertNull("'End' is not null", status.getEnd());
    }

    @Test
    public void test_datatypeConversion_HashMap() throws IOException, ChinoApiException {
        // Create UserSchema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("test_datatypeConversion_str", "string");
        fields.add(strField);
        Field intField = new Field ("test_datatypeConversion_int", "integer");
        fields.add(intField);

        UserSchemaStructure struc = new UserSchemaStructure(fields);
        UserSchemaRequest req = new UserSchemaRequest("UserSchema created for SchemaTest in Java SDK", struc);
        String userSchemaId = chino_admin.userSchemas.create(req).getUserSchemaId();

        // start datatype conversion
        HashMap<String, String> fieldsConversion = new HashMap<>();
        fieldsConversion.put(intField.getName(), "string");

        HashMap<String, Object> defaultValues = new HashMap<>();
        defaultValues.put(intField.getName(), "NOT_SET");

        // test with both fields, the strField should be ignored by the API because unchanged,
        // but should still be supported in the request body.
        DatatypeConversionStarted outcome = test.startDatatypeConversion(userSchemaId, fieldsConversion, defaultValues);
        assertNotNull(outcome);
        assertTrue(outcome.getMessage().contains("/v1/user_schemas/convert/" + userSchemaId + "/status"));

        // check status
        DatatypeConversionStatus status = test.getDatatypeConversionStatus(userSchemaId);
        assertNotNull("'status' is null", status);
        assertNotNull("'Msgs' is null", status.getMsgs());
        assertNotNull("'Percentage' is null", status.getPercentage());
        assertNotNull("'Start' is null", status.getStart());
        assertNotNull("'Status' is null", status.getStatus());

        if (status.getStatus() == ConversionStatus.COMPLETED)
            assertNotNull("'End' is null", status.getEnd());
        else
            assertNull("'End' is not null", status.getEnd());
    }

    @Test
    public void test_userSchemaDump() throws IOException, ChinoApiException, InterruptedException {
        // Create UserSchema
        List<Field> fields = new LinkedList<>();
        Field strField = new Field ("name", "string", true);
        fields.add(strField);
        UserSchemaStructure struc = new UserSchemaStructure(fields);
        UserSchemaRequest req = new UserSchemaRequest("UserSchema created for SchemaTest in Java SDK", struc);
        UserSchema s = chino_admin.userSchemas.create(req);
        // Create 4 users
        HashMap<String, String> userAttrs = new HashMap<>();
        userAttrs.put("name", "John");
        chino_admin.users.create("john", "testPassword123", userAttrs, s.getUserSchemaId(), true);
        userAttrs.put("name", "Paul");
        chino_admin.users.create("paul", "testPassword123", userAttrs, s.getUserSchemaId(), true);
        userAttrs.put("name", "Ringo");
        chino_admin.users.create("ringo", "testPassword123", userAttrs, s.getUserSchemaId(), true);
        userAttrs.put("name", "George");
        chino_admin.users.create("george", "testPassword123", userAttrs, s.getUserSchemaId(), true);

        List<String> fieldNames = new LinkedList<>();
        s.getStructure().getFields().forEach(field -> fieldNames.add(field.getName()));

        for (DumpFormat format : DumpFormat.values()) {
            System.out.println("# # # " + format + " dump # # #");
            // Start dump (async operation)
            System.out.print("Starting UserSchema dump... ");
            String dumpId = test.startUserSchemaDump(s.getUserSchemaId(), fieldNames,
                    SearchQueryBuilder.not("name", FilterOperator.EQUALS, "Paul"),
                    Collections.singletonList(new SortRule("name", SortRule.Order.ASC)),
                    format);
            System.out.println("Done.");
            assertNotNull(dumpId);

            // Check progress of async task
            DumpStatus status = DumpStatus.QUEUED;
            while (status != DumpStatus.COMPLETED) {
                System.out.print("Checking status... ");
                SchemaDumpStatus dumpProgress = test.checkUserSchemaDumpStatus(dumpId);
                status = dumpProgress.getStatus();
                System.out.println(status);
                // If the task failed, stop the test
                assertNotEquals(ConversionStatus.FAILED_CONVERSION, status);
                assertNotEquals(ConversionStatus.FAILED_REINDEX, status);
                // otherwise this check will be repeated every second
                Thread.sleep(1000);
            }

            // Download UserSchema dump
            System.out.print("Downloading dump... ");
            File dump = test.getUserSchemaDump(dumpId, TEST_DUMP_FOLDER, s.getUserSchemaId() + "-dump." + format);
            System.out.println("Done.");
            // check File was created
            System.out.print("Checking dump file... ");
            assertTrue(dump.exists());
            System.out.println("Found: " + dump.getAbsolutePath());
            // check content
            System.out.print("Checking file content... ");
            List<String> lines = Files.readAllLines(dump.toPath());
            StringBuilder content = new StringBuilder();
            lines.forEach(line -> content.append(line).append("\n"));
            assertTrue("'John' not found in:\n" + content.toString(), content.toString().contains("John"));
            assertTrue("'Ringo' not found in:\n" + content.toString(), content.toString().contains("Ringo"));
            assertTrue("'George' not found in:\n" + content.toString(), content.toString().contains("George"));
            // This Beatle is excluded by the "query" argument
            assertFalse(content.toString().contains("Paul"));
            System.out.println("Done.");
        }
    }

    private void doActivation(int methodId, String userSchemaId, UserSchemaStructure struct) throws IOException, ChinoApiException {
        switch (methodId) {
            case 0:
                UserSchemaRequest request = new UserSchemaRequest("test_activation_updated:" + methodId, struct);
                test.update(true, userSchemaId,
                        request
                );
                break;
            case 1:
                test.update(true, userSchemaId,
                        "test_activation_updated:" + methodId,
                        TestClassStructure.class
                );
                break;
            default:
                throw new IllegalArgumentException("Invalid iteration index: " + methodId);
        }
    }
}