package io.chino.java;

import io.chino.api.collection.Collection;
import io.chino.api.common.ChinoApiException;
import io.chino.api.common.Field;
import io.chino.api.document.Document;
import io.chino.api.schema.SchemaStructure;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.TestConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;

public class CollectionsTest extends ChinoBaseTest {

    private static ChinoAPI chino_admin;
    private static Collections test;

    private static String REPO_ID, SCHEMA_ID;

    @BeforeClass
    public static void beforeClass() throws IOException, ChinoApiException {
        ChinoBaseTest.runClass(CollectionsTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        test = ChinoBaseTest.init(chino_admin.collections);

        // create a repository and a Schema

        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.collections.list().getCollections().isEmpty(),
                chino_admin.collections
        ); // cleans repositories too

        REPO_ID = chino_admin.repositories.create("CollectionsTest"  + " [" + TestConstants.JAVA + "]")
                .getRepositoryId();

        LinkedList<Field> fields = new LinkedList<>();
        fields.add(new Field("title", "string", true));

        SCHEMA_ID = chino_admin.schemas.create(
                REPO_ID,
                "this Schema is used to verify that users are logged in and can create Documents.",
                new SchemaStructure(fields)
        ).getSchemaId();
    }

    @Test
    public void test_CRUD() throws Exception {
        /* CREATE */
        String collectionName = "Collection for testing Chino.io SDK";
        Collection c = test.create(collectionName);
        assertNotNull("Collection was not created! (null reference)", c);

        /* READ */
        Collection r = test.read(c.getCollectionId());
        assertEquals("Read object is different from original", c, r);

        /* UPDATE */
        Collection u = test.update(c.getCollectionId(), "UPDATED")
                        .getCollection();
        assertNotEquals("Object was not updated", c, u);
        assertEquals("Update failed", "UPDATED", u.getName());

        /* DELETE */
        test.delete(u.getCollectionId(), true);

        try {
            test.read(c.getCollectionId());
            fail("Object was not deleted.");
        } catch (ChinoApiException e) {
            System.out.println("Success");
        }
    }

    @Test
    public void test_documents() throws Exception {
        Document[] docs = {
                newDoc("test_documents_doc1"),
                newDoc("test_documents_doc2"),
                newDoc("test_documents_doc3")
        };
        Collection coll = test.create("test_documents");

        /* ADD */
        for (Document doc : docs) {
            test.addDocument(coll.getCollectionId(), doc.getDocumentId());
        }

        /* LIST DOCUMENTS */
        assertEquals("Not all documents have been added",
                docs.length,
                test.listDocuments(coll.getCollectionId()).getDocuments().size()
        );

        assertEquals("Wrong list size",
                1,
                test.listDocuments(coll.getCollectionId(), 2, 2).getDocuments().size()
        );

        assertEquals("Wrong list size",
                2,
                test.listDocuments(coll.getCollectionId(), 1, 2).getDocuments().size()
        );

        /* REMOVE */
        test.removeDocument(coll.getCollectionId(), docs[0].getDocumentId());
        List<Document> ls = test.listDocuments(coll.getCollectionId()).getDocuments();
        assertEquals("Document not removed",
                docs.length - 1,
                ls.size()
        );
        for (Document doc : ls) {
            test.removeDocument(coll.getCollectionId(), doc.getDocumentId());
        }

        ls = test.listDocuments(coll.getCollectionId()).getDocuments();
        assertEquals("Not all documents have been removed",
                0,
                ls.size()
        );
    }

    @Test
    public void test_list() throws Exception {
        Collection[] collections = {
                test.create("test_list_coll1"),
                test.create("test_list_coll2"),
                test.create("test_list_coll3"),
                test.create("test_list_coll4"),
                test.create("test_list_coll5")
        };
        /* LIST (no args) */
        assertEquals( "Missing collections in list",
                collections.length,
                test.list().getCollections().size()
        );

        /* LIST (2 args) */
        int offset = 0;
        int limit = 2;
        assertEquals( "Wrong list size (1)",
                limit,
                test.list(offset, limit).getCollections().size()
        );

        offset = collections.length - 1;
        limit = collections.length;
        assertEquals( "Wrong list size (2)",
                1,
                test.list(offset, limit).getCollections().size()
        );

        offset = 2;
        limit = collections.length;
        assertEquals( "Wrong list size (3)",
                limit - offset,
                test.list(offset, limit).getCollections().size()
        );
    }

    @Test
    public void test_listCollectionsOf() throws IOException, ChinoApiException {
        Document doc = newDoc("test_listCollectionsOf");

        Collection coll1 = test.create("test_listCollectionsOf_coll1");
        test.addDocument(coll1.getCollectionId(), doc.getDocumentId());

        Collection coll2 = test.create("test_listCollectionsOf_coll2");
        test.addDocument(coll2.getCollectionId(), doc.getDocumentId());

        Collection coll3 = test.create("test_listCollectionsOf_coll3");

        // list colls of the document (only the first 2 should be returned)
        LinkedList<String> filtered = new LinkedList<>();
        for (Collection coll : test.listCollectionsOf(doc.getDocumentId()).getCollections()) {
            filtered.add(coll.getCollectionId());
        }

        assertTrue("Coll. 1 not found!", filtered.contains(coll1.getCollectionId()));
        assertTrue("Coll. 2 not found!", filtered.contains(coll2.getCollectionId()));

        assertFalse("Coll. 3 should not be there!", filtered.contains(coll3.getCollectionId()));

        filtered.clear();

        // list colls of the document (with offset/limit)
        filtered = new LinkedList<>();
        for (Collection coll : test.listCollectionsOf(doc.getDocumentId(), 0, 1).getCollections()) {
            filtered.add(coll.getCollectionId());
        }
        assertEquals("Wrong size (1)", 1, filtered.size());

        assertTrue(filtered.contains(coll1.getCollectionId()) || filtered.contains(coll2.getCollectionId()));

        assertFalse("Coll. 3 should not be there!", filtered.contains(coll3.getCollectionId()));
    }

    @Test
    public void test_search() throws IOException, ChinoApiException {
        String collectionName = "test_search";
        Collection[] collections = {
                test.create(collectionName + "_coll1"),
                test.create(collectionName + "_coll2"),
                test.create(collectionName + "_coll3"),
                test.create(collectionName + "_coll4"),
                test.create(collectionName + "_coll5")
        };

        /* SEARCH (partial match) */
        List<Collection> filtered = test.search(collectionName, true).getCollections();

        assertEquals( "Wrong list size (" + collections.length + ")",
                collections.length, filtered.size());
        assertTrue(
                String.format(
                        "Filter by collection name returned a wrong result: \"%s\" does not contain \"%s\"",
                        filtered.get(0).getName(), collectionName
                ),
                filtered.get(0).getName().contains(collectionName)
        );

        filtered.clear();

        /* SEARCH (exact match) */
        filtered = test.search(collectionName, false).getCollections();
        assertEquals( "Wrong list size (0)", 0, filtered.size());
        filtered.clear();

        filtered = test.search(collections[0].getName(), false).getCollections();
        assertEquals( "Wrong list size (1)", 1, filtered.size());
        assertTrue(
                String.format(
                        "Filter by collection name returned a wrong result: \"%s\" does not contain \"%s\"",
                        filtered.get(0).getName(), collectionName
                ),
                filtered.get(0).getName().contains(collectionName)
        );
        filtered.clear();

        /* SEARCH (with pagination) */
        filtered = test.search(collectionName, true, 0, 3).getCollections();
        assertEquals( "Wrong list size (3)", 3, filtered.size());
        for (Collection result : filtered) {
            assertTrue(
                    String.format(
                            "Filter by collection name returned a wrong result: \"%s\" does not contain \"%s\"",
                            result.getName(), collectionName
                    ),
                    result.getName().contains(collectionName)
            );
        }
    }

    @Test
    public void test_activate() throws IOException, ChinoApiException {
        Collection before = test.create("test_activation");
        String id = before.getCollectionId();
        // Set is_active = false
        test.delete(id, false);
        assertFalse("Failed to set inactive", test.read(id).getIsActive());
        // Set is_active = true
        test.update(true, id, "test_activation_updated");
        Collection after = test.read(id);
        // Verify activation
        assertTrue("Failed to update value of 'is_active'", after.getIsActive());
        assertNotEquals("Failed to update values other than 'is_active'",
                before.getName(),
                after.getName()
        );

        test.delete(id, true);
    }

    private Document newDoc(String docTitle) throws IOException, ChinoApiException {
        HashMap<String, String> content = new HashMap<>();
        content.put("title", docTitle);

        return chino_admin.documents.create(SCHEMA_ID, content);
    }

}
