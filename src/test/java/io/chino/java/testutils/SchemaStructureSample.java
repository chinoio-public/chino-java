package io.chino.java.testutils;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;

@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class SchemaStructureSample {

    @JsonProperty("test_string")
    public String test_string;

    private SchemaStructureSample() {
    }

    public SchemaStructureSample(String testName) {
        this.test_string = testName;
    }

    public SchemaStructureSample(SchemaStructureSample copyFrom) {
        this.test_string = copyFrom.test_string;
    }

    public HashMap<String, Object> toHashMap() {
        HashMap<String, Object> hm = new HashMap<>();
        hm.put("test_string", test_string);

        return hm;
    }
}