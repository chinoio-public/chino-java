package io.chino.java.testutils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import okhttp3.Request;

/**
 * An implementation of {@link Interceptor} that can inspect the status of a request.
 * <br>
 * Usage:
 * <br>
 * <code>
 *      new TestInterceptor() {
 *          @Override public void checkRequest(Request req) {
 *              // Do something with the request, e.g.:
 *              Headers headers = req.headers();
 *              assertNotNull(headers.get("User-Agent"));
 *          }
 *      }
 *      .attachTo(someHttpClient)
 *      .testRequest(
 *          new Request.Builder()
 *              .get().url("https://www.example.com")
 *              .build()
 *      )
 *      .testRequest(
 *          new Request.Builder()
 *              .get().url("https://www.google.com")
 *              .build()
 *      );
 * </code>
 */
public abstract class TestInterceptor implements Interceptor {
    private OkHttpClient client = null;

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request req = chain.request();
        // run custom assertions/checks
        this.checkRequest(req);
        // proceed with the original request, 'req' may have been modified
        return chain.proceed(chain.request());
    }

    /**
     * Implement this method to perform custom checks and assertions on the {@link Request}.
     * 
     * @param req the Request to check
     */
    public abstract void checkRequest(Request req);

    /**
     * Provides a {@link OkHttpClient} that will be used to run 'testRequest'.
     * 
     * @param client the HTTP Client
     * 
     * @return this {@link TestInterceptor}
     */
    public TestInterceptor attachTo(OkHttpClient client) {
        this.client = client.newBuilder().addInterceptor(this).build(); 
        return this;
    }

    /**
     * Calls a request and runs the custom checks.
     * 
     * The request is actually sent to the server, so make sure it doesn't change sensitive data.
     * 
     * @param request the {@link Request} to test
     * 
     * @return this {@link TestInterceptor}
     * 
     * @throws IOException
     */
    public TestInterceptor testRequest(Request request) throws IOException {
        if (this.client == null) {
            throw new RuntimeException("No client was set up, did you call 'attachTo' before?");
        }
        this.client.newCall(request).execute();
        return this;
    }
}