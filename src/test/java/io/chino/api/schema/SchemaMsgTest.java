package io.chino.api.schema;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.chino.api.common.Field;
import io.chino.api.schema.*;
import io.chino.java.ChinoAPI;
import io.chino.java.ChinoBaseAPI;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.TestConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests that the new optional "msg" param is parsed properly.
 */
public class SchemaMsgTest {

    /**
     * Tests deserialization from JSON that includes the optional parameter "msg"
     */
    @Test
    public void testDeserializeWithMsg() throws IOException {
        String message = "Schema message";
        String json = "{\n" + //
                      "  \"result\": \"success\",\n" + //
                      "  \"result_code\": 200,\n" + //
                      "  \"message\": null,\n" + //
                      "  \"data\": {\n" + //
                      "    \"msg\": \""+ message + "\",\n" + //  This JSON contains the new field "msg"
                      "    \"schema\": {\n" + //
                      "      \"description\": \"Test schema\",\n" + //
                      "      \"schema_id\": \"11111111-1111-1111-1111-111111111111\",\n" + //
                      "      \"repository_id\": \"00000000-0000-0000-0000-000000000000\",\n" + //
                      "      \"is_active\": true,\n" + //
                      "      \"insert_date\": \"2024-01-01T00:00:00.000\",\n" + //
                      "      \"last_update\": \"2024-04-15T00:00:00.000\",\n" + //
                      "      \"structure\": {\n" + //
                      "        \"fields\": [\n" + //
                      "          {\n" + //
                      "            \"type\": \"str\",\n" + //
                      "            \"name\": \"field_str\"\n" + //
                      "          }\n" + //
                      "        ]\n" + //
                      "      }\n" + //
                      "    }\n" + //
                      "  }\n" + //
                      "}";

        GetSchemaResponse res = this.deserialize(json);
        // In this test, JSON has a "msg" inside "data"
        assertNotNull(res.getMsg());
        assertEquals(message, res.getMsg());
        
        // Check that the other fields are still handled properly
        Schema obj = res.getSchema();
        assertEquals("00000000-0000-0000-0000-000000000000", obj.getRepositoryId());
        assertEquals("11111111-1111-1111-1111-111111111111", obj.getSchemaId());
    }
    
    /**
     * Tests deserialization from JSON that doesn't include the optional parameter "msg"
     */
    @Test
    public void testDeserializeWithOutMsg() throws IOException {
        String json = "{\n" + //
                      "  \"result\": \"success\",\n" + //
                      "  \"result_code\": 200,\n" + //
                      "  \"message\": null,\n" + //
                      "  \"data\": {\n" + //
                      "    \"schema\": {\n" + //
                      "      \"description\": \"Test schema\",\n" + //
                      "      \"schema_id\": \"11111111-1111-1111-1111-111111111111\",\n" + //
                      "      \"repository_id\": \"00000000-0000-0000-0000-000000000000\",\n" + //
                      "      \"is_active\": true,\n" + //
                      "      \"insert_date\": \"2024-01-01T00:00:00.000\",\n" + //
                      "      \"last_update\": \"2024-04-15T00:00:00.000\",\n" + //
                      "      \"structure\": {\n" + //
                      "        \"fields\": [\n" + //
                      "          {\n" + //
                      "            \"type\": \"str\",\n" + //
                      "            \"name\": \"field_str\"\n" + //
                      "          }\n" + //
                      "        ]\n" + //
                      "      }\n" + //
                      "    }\n" + //
                      "  }\n" + //
                      "}";

        GetSchemaResponse res = this.deserialize(json);
        // In this test, JSON has no "msg" inside "data"
        assertNull(res.getMsg());
        
        // Check that the other fields are still handled properly
        Schema obj = res.getSchema();
        assertEquals("00000000-0000-0000-0000-000000000000", obj.getRepositoryId());
        assertEquals("11111111-1111-1111-1111-111111111111", obj.getSchemaId());
    }

    /**
     * Deserializes a json-formatted string using the same ObjectMapper from the {@link ChinoAPI} client
     * 
     * @param json a json-formatted String
     * 
     * @return The {@link GetSchemaResponse} from the json
     * 
     * @throws IOException JSON-deserialization errors
     */
    final GetSchemaResponse deserialize(String json) throws IOException {
        ObjectMapper mapper = ChinoBaseAPI.getMapper();
        JsonNode data = mapper.readTree(json).get("data");
        return mapper.convertValue(data, GetSchemaResponse.class);
    }
}
