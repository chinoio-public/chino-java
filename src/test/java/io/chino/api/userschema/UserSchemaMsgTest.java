package io.chino.api.userschema;

import java.io.IOException;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.chino.api.common.Field;
import io.chino.api.userschema.*;
import io.chino.java.ChinoBaseAPI;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.TestConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Tests that the new optional "msg" param is parsed properly.
 */
public class UserSchemaMsgTest {
    
    /**
     * Tests deserialization from JSON that includes the optional parameter "msg"
     */
    @Test
    public void testDeserializeWithMsg() throws IOException {
        String message = "UserSchema message";
        String json = "{\n" + //
                      "  \"result\": \"success\",\n" + //
                      "  \"result_code\": 200,\n" + //
                      "  \"message\": null,\n" + //
                      "  \"data\": {\n" + //
                      "    \"msg\": \""+ message + "\",\n" + //  This JSON contains the new field "msg"
                      "    \"user_schema\": {\n" + //
                      "      \"user_schema_id\": \"11111111-1111-1111-1111-111111111111\"," + //
                      "      \"description\": \"Test user schema\"," + //
                      "      \"is_active\": true," + //
                      "      \"insert_date\": \"2024-01-01T00:00:00.000\",\n" + //
                      "      \"last_update\": \"2024-04-15T00:00:00.000\",\n" + //
                      "      \"groups\": []," + //
                      "      \"structure\": {\n" + //
                      "        \"fields\": [\n" + //
                      "          {\n" + //
                      "            \"type\": \"str\",\n" + //
                      "            \"name\": \"field_str\"\n" + //
                      "          }\n" + //
                      "        ]\n" + //
                      "      }\n" + //
                      "    }\n" + //
                      "  }\n" + //
                      "}";

        GetUserSchemaResponse res = this.deserialize(json);
        // In this test, JSON has a "msg" inside "data"
        assertNotNull(res.getMsg());
        assertEquals(message, res.getMsg());
        
        // Check that the other fields are still handled properly
        UserSchema obj = res.getUserSchema();
        assertEquals("11111111-1111-1111-1111-111111111111", obj.getUserSchemaId());
    }
    
    /**
     * Tests deserialization from JSON that doesn't include the optional parameter "msg"
     */
    @Test
    public void testDeserializeWithOutMsg() throws IOException {
      String json = "{\n" + //
                    "  \"result\": \"success\",\n" + //
                    "  \"result_code\": 200,\n" + //
                    "  \"message\": null,\n" + //
                    "  \"data\": {\n" + //
                    "    \"user_schema\": {\n" + //
                    "      \"user_schema_id\": \"11111111-1111-1111-1111-111111111111\"," + //
                    "      \"description\": \"Test user schema\"," + //
                    "      \"is_active\": true," + //
                    "      \"insert_date\": \"2024-01-01T00:00:00.000\",\n" + //
                    "      \"last_update\": \"2024-04-15T00:00:00.000\",\n" + //
                    "      \"groups\": []," + //
                    "      \"structure\": {\n" + //
                    "        \"fields\": [\n" + //
                    "          {\n" + //
                    "            \"type\": \"str\",\n" + //
                    "            \"name\": \"field_str\"\n" + //
                    "          }\n" + //
                    "        ]\n" + //
                    "      }\n" + //
                    "    }\n" + //
                    "  }\n" + //
                    "}";

        GetUserSchemaResponse res = this.deserialize(json);
        // In this test, JSON has no "msg" inside "data"
        assertNull(res.getMsg());
        
        // Check that the other fields are still handled properly
        UserSchema obj = res.getUserSchema();
        assertEquals("11111111-1111-1111-1111-111111111111", obj.getUserSchemaId());
    }

    /**
     * Deserializes a json-formatted string using the same ObjectMapper from the {@link ChinoAPI} client
     * 
     * @param json a json-formatted String
     * 
     * @return The {@link GetUserSchemaResponse} from the json
     * 
     * @throws IOException JSON-deserialization errors
     */
    final GetUserSchemaResponse deserialize(String json) throws IOException {
        ObjectMapper mapper = ChinoBaseAPI.getMapper();
        JsonNode data = mapper.readTree(json).get("data");
        return mapper.convertValue(data, GetUserSchemaResponse.class);
    }
}