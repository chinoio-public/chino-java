package io.chino.api.common;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.InetAddress;
import java.util.concurrent.TimeUnit;

import org.junit.Test;

import io.chino.api.common.XForwardedForInterceptor;
import io.chino.java.testutils.TestConstants;
import io.chino.java.testutils.TestInterceptor;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * Test for {@link io.chino.api.common.XForwardedForInterceptor}.
 */
public class XForwardedForInterceptorTest {
    
    @Test
    public void testXForwardedForInterceptor__no_params() throws IOException {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS);

        // Use no-params constructor
        XForwardedForInterceptor interceptor = new XForwardedForInterceptor();
        String expectedIpAddr = InetAddress.getLocalHost().getHostAddress();

        builder.addInterceptor(interceptor);
        assertHeaderIsAdded(builder.build(), XForwardedForInterceptor.HEADER_NAME, expectedIpAddr);
    }
    
    @Test
    public void testXForwardedForInterceptor__String() throws IOException {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS);

        // Use String constructor
        String expectedIpAddr = "127.0.0.0";
        XForwardedForInterceptor interceptor = new XForwardedForInterceptor(expectedIpAddr);

        builder.addInterceptor(interceptor);
        assertHeaderIsAdded(builder.build(), XForwardedForInterceptor.HEADER_NAME, expectedIpAddr);
    }
    
    @Test
    public void testXForwardedForInterceptor__InetAddress() throws IOException {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS);

        // Use InetAddress constructor
        InetAddress expectedIpAddr = InetAddress.getByName("127.0.0.0");
        XForwardedForInterceptor interceptor = new XForwardedForInterceptor(expectedIpAddr);

        builder.addInterceptor(interceptor);
        assertHeaderIsAdded(builder.build(), XForwardedForInterceptor.HEADER_NAME, expectedIpAddr.getHostAddress());
    }

    private void assertHeaderIsAdded(OkHttpClient client, String headerName, String headerExpectedValue) throws IOException {
        new TestInterceptor() {
            @Override public void checkRequest(Request req) {
                Headers headers = req.headers();
                String headerActualValue = headers.get(headerName);
                
                String msg = String.format("Header \"%s\" not found in request", headerName);
                assertNotNull(msg, headerActualValue);

                msg = String.format(
                    "Wrong value for header \"%s\" in request: Expected \"%s\", found \"%s\".",
                    headerName, headerExpectedValue, headerActualValue
                );
                assertEquals(msg, headerExpectedValue, headerActualValue);
            }
        }
        .attachTo(client)
        .testRequest(
            new Request.Builder()
                .get().url("https://www.chino.io")
                .build()
        );
    }
    
}
