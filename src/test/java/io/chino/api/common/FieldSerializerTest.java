package io.chino.api.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.api.schema.Schema;
import io.chino.api.schema.SchemaStructure;
import io.chino.api.userschema.UserSchema;
import io.chino.api.userschema.UserSchemaStructure;
import io.chino.java.*;
import io.chino.java.testutils.ChinoBaseTest;
import io.chino.java.testutils.TestConstants;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;

import static org.junit.Assert.*;

public class FieldSerializerTest extends ChinoBaseTest {

    private static ChinoAPI chino_admin;
    private static Schemas schemas;
    private static UserSchemas userSchemas;

    private static String REPO_ID;

    private static final Field[] allFields = new Field[] {
            new Field("integer_field1", "integer"),
            new Field("integer_field2", "integer").withDefault(0),
            new Field("integer_field3", "integer").withDefault(1),
            new Field("integer_field4", "integer").withDefault(-2),
            new Field("integer_field5", "integer", true).withDefault(1),
            new Field("integer_field6", "integer").withDefault(null),

            new Field("integer_arr_field1", "array[integer]"),
            new Field("integer_arr_field2", "array[integer]").withDefault(0, 1, -2),
            new Field("integer_arr_field3", "array[integer]", true).withDefault(0, 1, -2),
            new Field("integer_arr_field4", "array[integer]").withDefault(null),
            new Field("integer_arr_field5", "array[integer]").withDefault(),  // empty array

            new Field("float_field1", "float"),
            new Field("float_field2", "float").withDefault(0.5),
            new Field("float_field3", "float").withDefault(1.4),
            new Field("float_field4", "float").withDefault(-2.3),
            new Field("float_field5", "float", true).withDefault(1.0),
            new Field("float_field6", "float").withDefault(null),

            new Field("float_arr_field1", "array[float]"),
            new Field("float_arr_field2", "array[float]").withDefault(0.5, 1.4, -2.3),
            new Field("float_arr_field3", "array[float]", true).withDefault(0.5, 1.4, -2.3),
            new Field("float_arr_field4", "array[float]").withDefault(null),
            new Field("float_arr_field5", "array[float]").withDefault(),  // empty array

            new Field("boolean_field1", "boolean"),
            new Field("boolean_field2", "boolean").withDefault(true),
            new Field("boolean_field3", "boolean").withDefault(false),
            new Field("boolean_field4", "boolean", true).withDefault(true),
            new Field("boolean_field3", "boolean").withDefault(null),

            new Field("string_field1", "string"),
            new Field("string_field2", "string").withDefault("default-string"),
            new Field("string_field3", "string", true).withDefault("default-string"),
            new Field("string_field2", "string").withDefault(null),

            new Field("string_arr_field1", "array[string]"),
            new Field("string_arr_field2", "array[string]").withDefault("s1", "s2", "s3"),
            new Field("string_arr_field3", "array[string]", true).withDefault("s1", "s2", "s3"),
            new Field("string_arr_field4", "array[string]").withDefault(null),
            new Field("string_arr_field5", "array[string]").withDefault(), // empty array

            new Field("text_field1", "text"),
            new Field("text_field2", "text").withDefault(
                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt " +
                            "ut labore et dolore magna aliqua. Sit amet massa vitae tortor condimentum lacinia quis. " +
                            "Odio ut enim blandit volutpat maecenas volutpat blandit aliquam. Ultrices mi tempus " +
                            "imperdiet nulla malesuada pellentesque elit eget. Egestas sed tempus urna et pharetra" +
                            " pharetra massa massa ultricies. Lectus proin nibh nisl condimentum. Arcu cursus " +
                            "euismod quis viverra nibh cras pulvinar. Ut tristique et egestas quis ipsum. Dui " +
                            "accumsan sit amet nulla facilisi morbi tempus iaculis. Molestie ac feugiat sed lectus " +
                            "vestibulum mattis ullamcorper velit."
            ),
            new Field("text_field3", "text").withDefault(null),

            new Field("date_field1", "date"),
            new Field("date_field2", "date").withDefault("1970-01-01"),
            new Field("date_field3", "date", true).withDefault("1970-01-01"),
            new Field("date_field4", "date").withDefault(null),

            new Field("time_field1", "time"),
            new Field("time_field2", "time").withDefault("00:01:30"),
            new Field("time_field3", "time", true).withDefault("00:01:30"),
            new Field("time_field4", "time").withDefault(null),

            new Field("datetime_field1", "datetime"),
            new Field("datetime_field2", "datetime").withDefault("1970-01-01T00:01:30"),
            new Field("datetime_field3", "datetime", true).withDefault("1970-01-01T00:01:30"),
            new Field("datetime_field4", "datetime").withDefault(null),

            new Field("json_field1", "json"),
            new Field("json_field2", "json").withDefault(defaultHashMap()),
            new Field("json_field4", "json").withDefault(null),
    };

    @BeforeClass
    public static void beforeClass() throws IOException, ChinoApiException {
        ChinoBaseTest.runClass(FieldSerializerTest.class);
        ChinoBaseTest.beforeClass();
        chino_admin = new ChinoAPI(TestConstants.HOST, TestConstants.CUSTOMER_ID, TestConstants.CUSTOMER_KEY);
        // no problem in calling init() twice, but it must be called at least once
        schemas = ChinoBaseTest.init(chino_admin.schemas);
        userSchemas = ChinoBaseTest.init(chino_admin.userSchemas);

        ChinoBaseTest.checkResourceIsEmpty(
                chino_admin.repositories.list().getRepositories().isEmpty(),
                chino_admin.repositories
        ); // cleans repositories too

        REPO_ID = chino_admin.repositories.create("SchemasTest" + " [" + TestConstants.JAVA + "]")
                .getRepositoryId();
    }

    @Test
    public void testSerialization() throws IOException {
        // Use the same ObjectMapper configuration used by the SDK classes to perform API calls
        ObjectMapper mapper = ChinoBaseAPI.getMapper();

        for (Field originalField : allFields) {
            // manually deserialize
            String valueAsString = mapper.writeValueAsString(originalField);
            Field deserializedField = mapper.readValue(valueAsString, Field.class);

            try {
                // Check that Field is equal after and before serialization
                // NOTE: do not use assertEquals because for some reason does not use Field.equals()
                assertTrue("Assertion failed for field " + originalField.getName(),
                        originalField.equals(deserializedField));

                // check that the serialized JSON String has the correct information
                assertNotEquals("Assertion failed for field " + originalField.getName(),
                        null, valueAsString);
                assertTrue("Assertion failed for field " + originalField.getName(),
                        valueAsString.contains(originalField.getName()));
                assertTrue("Assertion failed for field " + originalField.getName(),
                        valueAsString.contains(originalField.getType()));

                // Check that the default field value was properly serialized
                if (originalField.hasDefault()) {
                    assertTrue("Assertion failed for field " + originalField.getName(),
                            valueAsString.contains("\"default\""));
                } else {
                    assertFalse("Assertion failed for field " + originalField.getName(),
                            valueAsString.contains("\"default\""));
                }
            } catch (Exception e) {
                System.out.println("EXCEPTION DURING TEST - field information:");
                System.out.println(" - Field (Name, Type, Indexed, Default, Insensitive)");
                System.out.println(originalField.getName() + ", " + originalField.getType() + ", " + originalField.getIndexed() + ", " + originalField.getDefault() + ", " + originalField.getInsensitive());
                System.out.println(" - Field after (Name, Type, Indexed, Default, Insensitive)");
                System.out.println(deserializedField.getName() + ", " + deserializedField.getType() + ", " + deserializedField.getIndexed() + ", " + deserializedField.getDefault() + ", " + deserializedField.getInsensitive());
                System.out.println(" - JSON data");
                System.out.println(valueAsString);
            }
        }
    }

    @Test
    public void testFieldInSchema() throws IOException, ChinoApiException {
        for (Field originalField : allFields) {
            String schemaId = null;
            try {
                // CREATE
                SchemaStructure oneFieldStructure = new SchemaStructure().addField(originalField);
                schemaId = schemas.create(REPO_ID, "test field serialization: " + originalField.getName(), oneFieldStructure)
                        .getSchemaId();
                // READ
                Schema s = schemas.read(schemaId);
                SchemaStructure structure = s.getStructure();
                assertEquals("Assertion failed for field " + originalField.getName(),
                        1, structure.getFields().size());
                Field readField = structure.getFields().get(0);
                // NOTE: do not use assertEquals because for some reason does not use Field.equals()
                assertTrue("Assertion failed for field " + originalField.getName(),
                        readField.equals(originalField));
            } catch (ChinoApiException e) {
                // print the content of the Field, then throw exception again
                System.out.println("API EXCEPTION - INFORMATION:");
                System.out.println(" - Field (Name, Type, Indexed, Default, Insensitive)");
                System.out.println(originalField.getName() + ", " + originalField.getType() + ", " + originalField.getIndexed() + ", " + originalField.getDefault() + ", " + originalField.getInsensitive());
                throw e;
            }

            // delete the Schema
            if (schemaId != null) {
                try {
                    schemas.delete(schemaId, true);
                } catch (Exception e) {
                    System.err.println("Failed to delete Schema: " + schemaId);
                }
            }
        }
    }

    @Test
    public void testFieldInUserSchema() throws IOException, ChinoApiException {

        for (Field originalField : allFields) {
            String userSchemaId = null;
            try {
                // CREATE
                UserSchemaStructure oneFieldStructure = new UserSchemaStructure().addField(originalField);
                userSchemaId = userSchemas.create("test field serialization: " + originalField.getName(), oneFieldStructure)
                        .getUserSchemaId();
                // READ
                UserSchema s = userSchemas.read(userSchemaId);
                UserSchemaStructure structure = s.getStructure();
                assertEquals("Assertion failed for field " + originalField.getName(),
                        1, structure.getFields().size());
                Field readField = structure.getFields().get(0);
                // NOTE: do not use assertEquals because for some reason does not use Field.equals()
                assertTrue("Assertion failed for field " + originalField.getName(),
                        readField.equals(originalField));
            } catch (ChinoApiException e) {
                // print the content of the Field, then throw exception again
                System.out.println("API EXCEPTION - INFORMATION:");
                System.out.println(" - Field (Name, Type, Indexed, Default, Insensitive)");
                System.out.println(originalField.getName() + ", " + originalField.getType() + ", " + originalField.getIndexed() + ", " + originalField.getDefault() + ", " + originalField.getInsensitive());
                throw e;
            }

            // delete the UserSchema
            if (userSchemaId != null) {
                try {
                    userSchemas.delete(userSchemaId, true);
                } catch (Exception e) {
                    System.err.println("Failed to delete UserSchema: " + userSchemaId);
                }
            }
        }
    }

    private static HashMap<String, Object> defaultHashMap() {
        HashMap<String, Object> map = new HashMap<>();
        map.put("test", "map");  // for the 'json' field type
        return map;
    }
}
