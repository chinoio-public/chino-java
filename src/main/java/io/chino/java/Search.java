package io.chino.java;

import io.chino.api.search.DocumentsSearch;
import io.chino.api.search.UsersSearch;

/**
 * Run Search queries on the indexed data of Users and Documents.
 *
 * @see UsersSearch search Users
 * @see DocumentsSearch search Documents
 */
public class Search extends ChinoBaseAPI {

    /**
     * The default constructor used by all {@link ChinoBaseAPI} subclasses
     *
     * @param baseApiUrl      the base URL of the Chino.io API. For testing, use:<br>
     *                        {@code https://api.test.chino.io/v1/}
     * @param parentApiClient the instance of {@link ChinoAPI} that created this object
     */
    public Search(String baseApiUrl, ChinoAPI parentApiClient) {
        super(baseApiUrl, parentApiClient);
    }


    /**
     * Start a new Search among the Documents of a given Schema
     *
     * @param schemaId the ID of the Schema where the Documents must be looked for.
     *
     * @return a {@link DocumentsSearch} client that can build and execute a search query over Documents.
     */
    public DocumentsSearch documents(String schemaId) {
        return new DocumentsSearch(this, schemaId);
    }

    /**
     * Start a new Search among the Users of a given UserSchema
     *
     * @param userSchemaId the ID of the UserSchema where the Users must be looked for.
     *
     * @return a {@link UsersSearch} client that can build and execute a search query over Users.
     */
    public UsersSearch users(String userSchemaId) {
        return new UsersSearch(this, userSchemaId);
    }
}
