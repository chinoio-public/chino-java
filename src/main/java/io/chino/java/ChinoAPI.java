package io.chino.java;

import io.chino.api.common.LoggingInterceptor;
import io.chino.api.common.RequestDetailsInterceptor;
import io.chino.api.common.UnmodifiableList;
import io.chino.api.common.UserAgentInterceptor;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

/**
 * Main API client. Initializes and coordinates all the clients based on {@link ChinoBaseAPI}.
 */
public class ChinoAPI {

    /**
     * the version code of Chino.io API supported by this SDK
     */
    public static final String API_VERSION = "v1";

    /**
     * Prints information about every request. Not available agains production
     */
    private static boolean DEBUG_MODE = false;

    static final UserAgentInterceptor userAgent = new UserAgentInterceptor();
    static final Properties runtimeProperties = new Properties();

    OkHttpClient client;
    public Applications applications;
    public Auth auth;
    public UserSchemas userSchemas;
    public Schemas schemas;
    public Documents documents;
    public Repositories repositories;
    public Groups groups;
    public Collections collections;
    public Users users;
    public Search search;
    public Permissions permissions;
    public Blobs blobs;
    /**
     * @deprecated the Consent management API is deprecated in favor of the <a href="https://docs.chino.io/consent/consentame/docs/v1">Consenta API</a>.
     *             This code will be removed in the upcoming major version.
     */
    @Deprecated
    public Consents consents;

    /**
     * A list of custom {@link Interceptor Interceptors} that allow to pass custom headers in the requests.
     * 
     * @see #addCustomInterceptor(Interceptor)
     * @see #getCustomInterceptors()
     * @see #resetCustomInterceptors()
     */
    private List<Interceptor> customInterceptors = new LinkedList<>();

    /**
     * Construct an API client which authenticates calls with a {@code (customerID, customerKey)} pair.
     * To be used <b>only in secure clients</b>.
     *
     * @param hostUrl the base URL for the API calls. (will be forced to use 'https://')
     * @param customerId the customer id provided by Chino.io
     * @param customerKey the customer key provided by Chino.io
     */
    public ChinoAPI(String hostUrl, String customerId, String customerKey) {
        checkNotNull(hostUrl, "host_url");
        checkNotNull(customerId, "customer_id");
        checkNotNull(customerKey, "customer_key");
        updateHttpAuth(new LoggingInterceptor(customerId, customerKey));
        initObjects(hostUrl);
    }

    /**
     * Construct an unauthenticated API client.
     * Mainly used for client-side applications.
     * Users will need to authenticate via {@link Auth ChinoAPI.auth} using username and password or an authentication code
     *
     * @param hostUrl the base URL for the API calls
     */
    public ChinoAPI(String hostUrl) {
        checkNotNull(hostUrl, "host_url");
        updateHttpAuth(null);
        initObjects(hostUrl);
    }
    
    /**
     * Construct an API client which authenticates calls with a bearer token.
     * Mainly used for server-side applications.
     * @param hostUrl the base URL for the API calls
     * @param bearerToken the bearer token to use for further calls
     */
    public ChinoAPI(String hostUrl, String bearerToken) {
        checkNotNull(hostUrl, "host_url");
        updateHttpAuth(new LoggingInterceptor(bearerToken));
        initObjects(hostUrl);
    }
    
    private void initObjects(String hostUrl){
        hostUrl = normalizeApiUrl(hostUrl);
        applications = new Applications(hostUrl, this);
        userSchemas = new UserSchemas(hostUrl, this);
        documents = new Documents(hostUrl, this);
        schemas = new Schemas(hostUrl, this);
        repositories = new Repositories(hostUrl, this);
        groups = new Groups(hostUrl, this);
        collections = new Collections(hostUrl, this);
        users = new Users(hostUrl, this);
        search = new Search(hostUrl, this);
        auth = new Auth(hostUrl, this);
        permissions = new Permissions(hostUrl, this);
        blobs = new Blobs(hostUrl, this);
        consents = new Consents(hostUrl, this);  // todo: will be removed in the upcoming major version
    }

    private void checkNotNull(Object object, String name){
        if(object == null){
            throw new NullPointerException(name);
        }
    }

    /**
     * Check format of the API host url and append the {@link #API_VERSION} code
     * if required.
     *
     * @param hostUrl the url to Chino.io API
     * @return the polished URL with {@link #API_VERSION} code and without trailing '/'
     */
    private static String normalizeApiUrl(String hostUrl) {
        String normalizedHostUrl = hostUrl;

        // force https
        if (normalizedHostUrl.startsWith("http://")) {
            if (normalizedHostUrl.contains(".chino.io")) {
                normalizedHostUrl = normalizedHostUrl.replace("http://", "https://");
            }
        }

        // fix version number
        while (normalizedHostUrl.endsWith("/")) {
            // remove trailing '/' (if any) - if more than one is there, removes all of them.
            normalizedHostUrl = normalizedHostUrl.replaceFirst("/$", "");
        }
        if (!normalizedHostUrl.endsWith(API_VERSION)) {
            normalizedHostUrl += "/v1";
        }

        return normalizedHostUrl;
    }

    /**
     * Get the default HTTP client.
     * 
     * NOTE: the {@code customInterceptors} can not be used to modify the UserAgent.
     * 
     * @param customInterceptors Optional list of {@link Interceptor Interceptors}, 
     *   e.g. to add custom headers to every request done by the SDK
     * 
     * @return
     */
    static final OkHttpClient.Builder getDefaultHttpClient(List<Interceptor> customInterceptors) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
            .connectTimeout(10, TimeUnit.SECONDS)
            .writeTimeout(10, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS);

        if (customInterceptors != null) {
            for (Interceptor i : customInterceptors) {
                builder = builder.addInterceptor(i);
            }
        }

        // Keep at the end, overwriting the user agent is not allowed
        return builder.addInterceptor(userAgent);
    }

    /**
     * Get the default HTTP client.
     * 
     * @return
     */
    static final OkHttpClient.Builder getDefaultHttpClient() {
        return getDefaultHttpClient(null);
    }

    final OkHttpClient getHttpClient() {
        return client;
    }

    final void updateHttpAuth(LoggingInterceptor authInterceptor) {
        OkHttpClient.Builder builder = getDefaultHttpClient(this.customInterceptors);

        if (authInterceptor != null) {
            builder = builder.addNetworkInterceptor(authInterceptor);
        }

        // Add the debugging Interceptor LAST
        String debugRequests = runtimeProperties.getProperty("chino.debug.request.print_all", "false");
        String hideAuthProperty = runtimeProperties.getProperty("chino.debug.request.hide_auth", "true");
        Boolean sanitizeAuthHeaders = hideAuthProperty.equals("true");
        if (debugRequests.toLowerCase().equals("true")) {
            System.out.println("------ WARNING ------------------------------------------------------");
            System.out.println("You are running with 'chino.debug.request.print_all=true'.");
            System.out.println("All requests will be printed to System.out");
            if (sanitizeAuthHeaders) 
                System.out.println("Set 'chino.debug.request.hide_auth=false' to show Auth headers.");
            System.out.println("------ IMPORTANT ----------------------------------------------------");
            System.out.println("TURN THIS OFF IN PRODUCTION to prevent leaking sensitive data.");
            System.out.println();
            builder = builder.addNetworkInterceptor(new RequestDetailsInterceptor(sanitizeAuthHeaders));
        }
        
        this.client = builder.build();
    }

    /**
     * Append a client name to the "User-Agent" HTTP header.
     *
     * @param name the client name. If null, the appended value will be removed.
     */
    public final void setClientName(String name) {
        userAgent.updateClientName(name);
    }

    /**
     * Get the client name which is sent within the "User-Agent" HTTP header.
     *
     * @return the current client name
     */
    public final String getClientName() {
        return userAgent.getClientName();
    }

    /**
     * Get the content of the "User-Agent" HTTP header.
     *
     * @return the value which is written in the header.
     */
    public final String getUserAgent() {
        return userAgent.getUserAgent();
    }

    /**
     * Change this ChinoAPI authentication method to Bearer token
     * @param token the new bearer token that will be used to authenticate API calls to Chino.io API
     *
     * @return this {@link ChinoAPI} client with the new authentication method
     */
    public ChinoAPI setBearerToken(String token) {
        checkNotNull(token, "token");
        updateHttpAuth(new LoggingInterceptor(token));
        return this;
    }

    /**
     * Change this ChinoAPI authentication method to Basic Auth,
     * using the customer credentials. <br>
     * <br>
     * <b>WARNING: don't use this in public {@link Applications}</b>
     *
     * @param customerId the Chino.io customer ID
     * @param customerKey the Chino.io customer key
     *
     * @return this {@link ChinoAPI} client with the new authentication method
     */
    public ChinoAPI setCustomer(String customerId, String customerKey) {
        checkNotNull(customerId, "customer id");
        checkNotNull(customerKey, "customer key");
        updateHttpAuth(new LoggingInterceptor(customerId, customerKey));
        return this;
    }

    /**
     * Adds a custom {@link Interceptor} to the list of custom Interceptors of this client instance.
     * <br>
     * Custom interceptors allow, for example, to send custom request headers.
     * <br>
     * These will affect every request sent by the SDK.
     * 
     * @param interceptor The custom {@link Interceptor} to add.
     */
    public void addCustomInterceptor(Interceptor interceptor) {
        this.customInterceptors.add(interceptor);
        // refresh the local HTTP client without changing auth method
        this.updateHttpAuth(null);
    }

    /**
     * Removes any custom {@link Interceptor} added to this client instance.
     */
    public void resetCustomInterceptors() {
        this.customInterceptors.clear();
    }

    /**
     * @return a read-only list of the custom Interceptors active on this client instance.
     */
    public List<Interceptor> getCustomInterceptors() {
        return new UnmodifiableList<Interceptor>(this.customInterceptors);
    }

    /**
     * Get a {@link List} of all versions of Chino.io API supported by this SDK
     * (now only "v1")
     *
     * @return a {@link List List&lt;String&gt;} with the supported version codes
     */
    public static List<String> getAvailableVersions() {
        return java.util.Collections.singletonList(API_VERSION);
    }

    public static void loadProperties(String filePath) {
        try {
            // attempt to load Properties file
            runtimeProperties.load(new FileReader(filePath));
        } catch (IOException e) {
            System.err.println("Failed to load runtime properties from '" + filePath + ". Caused by:");
            e.printStackTrace(System.err);
            System.err.flush();
        }
    }
}
