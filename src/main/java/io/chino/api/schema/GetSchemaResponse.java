
package io.chino.api.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.logging.Logger;


/**
 * Wraps an {@link Schema} returned as a response to an API call
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "result",
    "schema",
    "msg"
})
public class GetSchemaResponse {

    @JsonProperty("result")
    private String result;
    @JsonProperty("schema")
    private Schema schema;
    @JsonProperty("msg")
    private String msg;

    /**
     * 
     * @return
     *     The result
     */
    @JsonProperty("result")
    public String getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    @JsonProperty("result")
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * 
     * @return
     *     The schema
     */
    @JsonProperty("schema")
    public Schema getSchema() {
        return schema;
    }

    /**
     * 
     * @param schema
     *     The schema
     */
    @JsonProperty("schema")
    public void setSchema(Schema schema) {
        this.schema = schema;
    }

    /**
     * 
     * @return
     *     The msg if it's defined, otherwise returns {@code null}.
     */
    @JsonProperty("msg")
    public String getMsg() {
        return msg;
    }

    /**
     * 
     * @param msg
     *    The msg
     */
    @JsonProperty("msg")
    public void setMsg(String msg) {
        this.msg = msg;

        // If msg is not empty, log it to the application logs
        if (msg != null && !msg.equals("")) {
            String logMsg = "Received Schema msg from Chino API: '" + msg + "'.";
            if (this.getSchema() != null) {
                logMsg += " Schema ID: " + this.getSchema().getSchemaId();
            }
            Logger.getLogger(this.getClass().getName()).warning(logMsg);
        }
    }

}
