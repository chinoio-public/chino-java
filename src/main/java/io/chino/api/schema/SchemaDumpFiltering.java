package io.chino.api.schema;

import com.fasterxml.jackson.core.type.TypeReference;
import io.chino.api.common.DumpFormat;
import io.chino.api.search.AbstractSearchClient;
import io.chino.api.search.ResultType;
import io.chino.api.search.SearchQueryBuilder;
import io.chino.api.search.SortRule;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public class SchemaDumpFiltering extends AbstractSearchClient<String> {
    private final LinkedList<String> fields;
    private DumpFormat format;

    @Deprecated
    public SchemaDumpFiltering(Collection<String> fields, SearchQueryBuilder query, List<SortRule> sort, io.chino.api.schema.DumpFormat format) {
        // use DumpFormat from io.chino.api.common
        // if format==null, JSON is the default
        this(fields, query, sort, (format == io.chino.api.schema.DumpFormat.CSV) ? DumpFormat.CSV : DumpFormat.JSON);
    }

    public SchemaDumpFiltering(Collection<String> fields, SearchQueryBuilder query, List<SortRule> sort, DumpFormat format) {
        super();
        this.fields = new LinkedList<>(fields);
        // format is optional
        if (format == null) format = DumpFormat.JSON;
        this.format = format;
        // sort is optional
        if (sort == null) sort = new LinkedList<>();
        for (SortRule sortRule : sort) {
            this.addSortRule(sortRule);
        }
        // Initialize query in this object
        this.with(query).buildSearch();
    }

    @Override
    public String execute(int offset, int limit) {
        throw new UnsupportedOperationException(
                "Instances of SchemaDumpFiltering can not be executed."
        );
    }

    public SchemaDumpFiltering setFormat(DumpFormat format) {
        this.format = format;
        return this;
    }

    public HashMap<String, Object> toHashMap() throws IOException {
        // Get the JSON from the superclass, it will only contain fields "query" and "sort" like the basic search query
        String partialRequest = parseSearchRequest();
        // Convert to a HashMap
        HashMap<String, Object> map = mapper.readValue(partialRequest, new TypeReference<HashMap<String, Object>>() {});
        // Keep only fields supported in Schema Dump operations
        map.remove("result_type");
        map.put("fields", this.fields);
        map.put("format", this.format.toString());
        return map;
    }

    // override unused AbstractSearchClient functionalities

    @Override
    public SchemaDumpFiltering setResultType(ResultType resultType) {
        // no changes, dump operations don't support result_type
        return this;
    }

    @Override
    public SchemaDumpFiltering addSortRule(String fieldName, SortRule.Order order) {
        super.addSortRule(fieldName, order);
        return this;
    }

    @Override
    public SchemaDumpFiltering addSortRule(String fieldName, SortRule.Order order, int index) {
        super.addSortRule(fieldName, order, index);
        return this;
    }

}
