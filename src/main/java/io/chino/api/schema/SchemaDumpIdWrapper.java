package io.chino.api.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.chino.api.search.SearchQueryBuilder;

import java.util.List;

/**
 *  Wraps the dumpId returned by Chino.io API when starting a Schema Dump operation
 *  
 * @see io.chino.java.Schemas#startSchemaDump(String, List, SearchQueryBuilder, List, DumpFormat)
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SchemaDumpIdWrapper {

    @JsonProperty("dump_id")
    private String dumpId;

    public String getDumpId() {
        return dumpId;
    }
}
