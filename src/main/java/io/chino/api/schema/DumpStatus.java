package io.chino.api.schema;

/**
 * The list of possible statuses of Schema Dump operations
 */
public enum DumpStatus {
    QUEUED,
    STARTING,
    DUMPING,
    INDEXING,
    COMPLETED,
    FAILED_CONVERSION,
    FAILED_REINDEX,
}
