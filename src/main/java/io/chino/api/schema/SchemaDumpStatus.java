package io.chino.api.schema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.Date;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "dump_id",
        "start_date",
        "last_update",
        "status",
        "records_count",
        "size",
        "sha256"
})
public class SchemaDumpStatus {

    @JsonProperty("dump_id")
    private String dumpId;
    @JsonProperty("start_date")
    private Date startDate;
    @JsonProperty("last_update")
    private Date lastUpdate;
    @JsonProperty("status")
    private DumpStatus status;
    @JsonProperty("records_count")
    private Integer recordsCount;
    @JsonProperty("size")
    private Integer size;
    @JsonProperty("sha256")
    private String sha256;

    @JsonProperty("dump_id")
    public String getDumpId() {
        return dumpId;
    }

    @JsonProperty("dump_id")
    public void setDumpId(String dumpId) {
        this.dumpId = dumpId;
    }

    @JsonProperty("start_date")
    public Date getStartDate() {
        return startDate;
    }

    @JsonProperty("start_date")
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("last_update")
    public Date getLastUpdate() {
        return lastUpdate;
    }

    @JsonProperty("last_update")
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    @JsonProperty("status")
    public DumpStatus getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(DumpStatus status) {
        this.status = status;
    }

    @JsonProperty("records_count")
    public Integer getRecordsCount() {
        return recordsCount;
    }

    @JsonProperty("records_count")
    public void setRecordsCount(Integer recordsCount) {
        this.recordsCount = recordsCount;
    }

    @JsonProperty("size")
    public Integer getSize() {
        return size;
    }

    @JsonProperty("size")
    public void setSize(Integer size) {
        this.size = size;
    }

    @JsonProperty("sha256")
    public String getSha256() {
        return sha256;
    }

    @JsonProperty("sha256")
    public void setSha256(String sha256) {
        this.sha256 = sha256;
    }

}
