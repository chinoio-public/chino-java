package io.chino.api.common;

import com.fasterxml.jackson.annotation.*;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import io.chino.api.common.serializers.FieldSerializer;

import java.util.*;

/**
 * This class represents a field of a {@link io.chino.api.userschema.UserSchema UserSchema}
 * or a {@link io.chino.api.schema.Schema Schema}. It is used to create or update the structure
 * of these objects.
 */
@JsonInclude(JsonInclude.Include.NON_DEFAULT)
@JsonPropertyOrder({
        "type",
        "name",
        "indexed",
        "insensitive",
        "default"
})
@JsonSerialize(using = FieldSerializer.class)
public class Field {
    @JsonProperty("type")
    private String type;
    @JsonProperty("name")
    private String name;
    @JsonProperty("indexed")
    private Boolean indexed;
    @JsonProperty("insensitive")
    private Insensitive insensitive = Insensitive.NOT_SET;
    /*
     *
     * called 'defaultValue' because 'default' is a reserved Java keyword.
     * Getter and setter methods are named after the "default" property (not 'defaultValue').
     */
    @JsonProperty("default")
    private Object defaultValue = Default.NOT_SET;

    /**
     * Base constructor used in serialization
     */
    public Field() {
    }

    /**
     * Create a {@link Field}
     *
     * @param name the name of the Field
     * @param type the data type of the Field
     */
    public Field(String name, String type) {
        setName(name);
        setType(type);
    }

    /**
     * Create a {@link Field} that can be indexed for Search
     *
     * @param name the name of the Field
     * @param type the data type of the Field
     * @param indexed {@code true} if the Field must be indexed for Search.
     *                           Only available for specific "type"
     */
    public Field(String name, String type, Boolean indexed) {
        setName(name);
        setType(type);
        setIndexed(indexed);
    }

    /**
     * Create a {@link Field} that can be indexed for Search.
     *
     * @param name the name of the Field
     * @param type the data type of the Field
     * @param indexed {@code true} if the Field must be indexed for Search.
     *                           Only available for specific "type"
     * @param insensitive {@code true} if the Field must be indexed in a case-insensitive fashion.
     *                               Only available for specific "type"
     */
    public Field(String name, String type, Boolean indexed, Boolean insensitive) {
        setName(name);
        setType(type);
        setIndexed(indexed);
        setInsensitive(insensitive);
    }

    /**
     * @return the "type" of this {@link Field}
     */
    @JsonProperty("type")
    public String getType() {
        return type;
    }

    /**
     *
     * @param type the "type" of this {@link Field}
     */
    @JsonProperty("type")
    public final void setType(String type) {
        if (type == null) {
            throw new NullPointerException("type");
        }
        this.type = type;
    }

    /**
     * @return the "name" of this {@link Field}
     */
    @JsonProperty("name")
    public String getName() {
        return name;
    }

    /**
     *
     * @param name the "name" of this {@link Field}
     */
    @JsonProperty("name")
    public final void setName(String name) {
        if (name == null) {
            throw new NullPointerException("name");
        }
        this.name = name;
    }

    /**
     *
     * @return the "indexed" status of this {@link Field} as a {@link Boolean}
     */
    public Boolean getIndexed() {
        return indexed;
    }

    /**
     *
     * @param indexed a {@link Boolean} representing whether or not this {@link Field} must be indexed.
     *               Default is {@code false}. May <b>NOT</b> be {@code null}.
     */
    public final void setIndexed(Boolean indexed) {
        if (indexed == null) {
            throw new NullPointerException("indexed");
        }
        this.indexed = indexed;
    }

    /**
     *
     * @return the value of the "insensitive" property.
     * <br>
     * <b>NOTE:</b> may be {@code null} if the property is not set for this {@link Field}.
     *
     * @see #clearInsensiive()
     * @see #setInsensitive(Boolean)
     */
    public Boolean getInsensitive() {
        return insensitive.toBoolean();
    }

    /**
     * Set the property "insensitive" in this {@link Field}
     *
     * @param insensitive a Boolean value. May <b>NOT</b> be {@code null}: if you want to unset the property
     *                    for this {@link Field}, use {@link #clearInsensiive()}
     *
     * @see #clearInsensiive()
     * @see #getInsensitive()
     */
    public final void setInsensitive(Boolean insensitive) {
        if (insensitive == null) {
            throw new NullPointerException("insensitive");
        }
        this.insensitive = Insensitive.fromBoolean(insensitive);
    }

    /**
     * Removes the property "insensitive" from this {@link Field}.
     *
     * @see #setInsensitive(Boolean)
     * @see #getInsensitive()
     */
    public final void clearInsensiive() {
        this.insensitive = Insensitive.NOT_SET;
    }

    /**
     * Set the value of the 'default' property of the {@link Field}.<br>
     * <b>NOTE:</b> the 'default property supports {@code null} as value.
     * If you don't want to include the 'default' property in this object, use {@link #clearDefault()}.
     *
     * @param defaultValue value to set as 'default' for this Field, when it's not specified in the API call to
     *                     create a Document/User. <b>May be {@code null}</b>.
     *
     * @see #withDefault(Object) withDefault() for in-place instantiation
     */
    @JsonProperty("default")
    public void setDefault(Object defaultValue) {
        /*
         * If the default value is a List, comparison of 2 Fields with Field.equals() requires that they
         * have the same class. For this reason, we wrap any List or Array in an ArrayList, since ArrayList is also
         * the class used by Jackson to deserialize JSON lists to Java object.
         *
         *  An ArrayList is required to be able to compare one Field created locally and another received via JSON from the API.
         */
        if (defaultValue instanceof List) {
            defaultValue = new ArrayList((List) defaultValue);
        }

        // finally, set the value
        this.defaultValue = defaultValue;
    }

    /**
     * Set the value of the 'default' property of the {@link Field} that have
     * {@code "type" = "array[int/float/string]"}.
     * <br>
     * You can use this method only on <b>fields that have {@code "type" = "array[int/float/string]"}</b>.
     * <br>
     * For any other type, use {@link #withDefault(Object)}.
     * <br>
     * <b>NOTE:</b> the 'default property supports {@code null} as value.
     * If you don't want to include the 'default' property in this object, use {@link #clearDefault()}.
     *
     * @param defaultValues value to set as 'default' for this Field, when it's not specified in the API call to
     *                     create a Document/User. <b>May be {@code null}</b>.
     *
     * @see #withDefault(Object) withDefault() for in-place instantiation
     */
    public void setDefault(Object ... defaultValues) {
        /*
         * If the default value is an array, comparison of 2 Fields with Field.equals() requires that they
         * have the same class. For this reason, we wrap any List or Array in an ArrayList, since ArrayList is also
         * the class used by Jackson to deserialize JSON lists to Java object.
         *
         * An ArrayList is required to be able to compare one Field created locally and another received via JSON from the API.
         */
        if (defaultValues != null) {
            List defaultValueAsList = Arrays.asList(defaultValues);
            setDefault(defaultValueAsList);
        } else {
            // for some reason, with null this method is used instead of the base setDefault()
            this.defaultValue = null;
        }
    }

    /**
     * Remove the 'default' property in the field.
     *
     * @return this {@link Field} instance, for in-place instantiation
     */
    public Field clearDefault() {
        this.defaultValue = Default.NOT_SET;
        return this;
    }

    /**
     * Set the value of the 'default' property of the {@link Field}.<br>
     * <b>NOTE:</b> the 'default property supports {@code null} as value.
     * If you don't want to include the 'default' property in this object, use {@link #clearDefault()}.
     *
     * @param defaultValue value to set as 'default' for this Field, when it's not specified in the API call to
     *                     create a Document/User. <b>May be {@code null}</b>.
     *
     * @return this {@link Field} instance, for in-place instantiation
     * 
     * @see #setDefault(Object)
     * @see #clearDefault()
     */
    public Field withDefault(Object defaultValue) {
        setDefault(defaultValue);
        return this;
    }

    /**
     * Set the value of the 'default' property of the {@link Field}.
     * <br>
     * You can use this method only on <b>fields that have {@code "type" = "array[int/float/string]"}</b>.
     * <br>
     * For any other type, use {@link #withDefault(Object)}.
     * <br>
     * <b>NOTE:</b> the 'default property supports {@code null} as value.
     * If you don't want to include the 'default' property in this object, use {@link #clearDefault()}.
     *
     * @param defaultValues value to set as 'default' for this Field, when it's not specified in the API call to
     *                     create a Document/User. <b>May be {@code null}</b>.
     *
     * @return this {@link Field} instance, for in-place instantiation
     *
     * @see #withDefault(Object)
     * @see #clearDefault()
     */
    public Field withDefault(Object ... defaultValues) {
        /*
         * Duplicated to handle the conversion from array to List - see setDefault()
         */
        setDefault(defaultValues);
        return this;
    }

    /**
     * Read the default value of this {@link Field}.
     * <br>
     * This method may return:
     * <ul>
     *     <li>
     *         An {@link Object}, which was set as the "default" value for this {@link Field}
     *     </li>
     *     <li>
     *         {@code null}, which means that the {@code null} value was set as a default for this {@link Field}
     *     </li>
     *     <li>
     *         The enum constant {@link Default#NOT_SET}, which means that the "default" property was NOT defined
     *         for this {@link Field}
     *     </li>
     * </ul>
     *
     * @return the current "default" value
     *
     * @see #hasDefault() Use hasDefault() to check if a default value was set for this Field
     */
    @JsonProperty("default")
    public Object getDefault() {
        return defaultValue;
    }

    /**
     *
     * @return {@code true} if a "default" value is defined for this {@link Field}. The default may be
     */
    public boolean hasDefault() {
        return this.defaultValue != Default.NOT_SET;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Field field = (Field) o;
        return Objects.equals(type, field.type) &&
                Objects.equals(name, field.name) &&
                Objects.equals(indexed, field.indexed) &&
                Objects.equals(insensitive, field.insensitive) &&
                Objects.equals(defaultValue, field.defaultValue);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, name, indexed, insensitive);
    }



    /* ENUMS */


    /**
     * Defines the {@link #NOT_SET} value for the "default" property.
     * <br>
     * The "default" property may have a {@code null} value, but it is also optional.
     * This value is set in the Java attribute when no value is spedcified for the "default" property
     */
    public enum Default {
        /**
         * The "default" property may have a {@code null} value, but it is also optional.
         * This value is set in the Java attribute when no value is specified for the "default" property
         */
        NOT_SET
    }

    /**
     * Allowed values for the "insensitive" property. Since the property may be undefined, we need 3 states
     * instead of a boolean value.
     *
     * Defining an enum is more elegant than using {true, false, null} as the allowed values and doesn't break
     * compatibility with past versions of the SDK.
     */
    public enum Insensitive {
        TRUE,
        FALSE,
        NOT_SET;

        public Boolean toBoolean() {
            switch (this) {
                case TRUE: return true;
                case FALSE: return false;
                case NOT_SET:
                default:
                    return null;
            }
        }

        public static Insensitive fromBoolean(Boolean value) {
            if (value == null)
                return NOT_SET;

            if (value) return TRUE;
            else return FALSE;
        }
    }
}
