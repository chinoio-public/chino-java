package io.chino.api.common;

import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * An {@link Interceptor} that adds an header "X-Forwarded-For" to every request.
 * 
 * The value of this header is the IP address of the host making the call, which can
 * be forwarded behind a firewall or proxy to determine the source IP of the original call.
 * 
 * <i>Based on the suggestion from Thomas Zezula (upheal.io).</i>
 */
public class XForwardedForInterceptor implements Interceptor {
    public static final String HEADER_NAME = "X-Forwarded-For";
    private InetAddress clientIpAddress;

    /**
     * Creates a {@link XForwardedForInterceptor} for localhost
     * 
     * @throws UnknownHostException Unable to read host IP address. 
     *   To handle the exception outside of this constructor, fetch the {@link InetAddress}
     *   first and pass it to {@link #XForwardedForInterceptor(InetAddress)}.
     */
    public XForwardedForInterceptor() throws UnknownHostException {
        this(InetAddress.getLocalHost());
    }

    /**
     * Creates a {@link XForwardedForInterceptor} for the specified IP address.
     *
     * @param clientIpAddress the IP address
     * 
     * @throws UnknownHostException The {@link String} contains a malformed IP address.
     *   To handle the exception outside of this constructor, convert the string to an 
     *   {@link InetAddress} first and pass it to {@link #XForwardedForInterceptor(InetAddress)}.
     */
    public XForwardedForInterceptor(String clientIpAddress) throws UnknownHostException {
        this(InetAddress.getByName(clientIpAddress));
    }

    /**
     * Creates a {@link XForwardedForInterceptor} for the specified {@link InetAddress}.
     * 
     * @param clientIpAddress the IP address
     */
    public XForwardedForInterceptor(InetAddress clientIpAddress) {
        this.clientIpAddress = clientIpAddress;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request()
                .newBuilder()
                .header(HEADER_NAME, getClientIpAddress())
                .build();

        return chain.proceed(request);
    }

    /**
     * 
     * @return the IP address as a {@link String}.
     */
    private String getClientIpAddress() {
        return this.clientIpAddress.getHostAddress();
    }   
}
