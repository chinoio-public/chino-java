package io.chino.api.common;

/**
 * The list of possible statuses of Datatype Conversion operations on Schemas and UserSchemas
 */
public enum ConversionStatus {
    QUEUED,
    STARTING,
    CONVERTING,
    INDEXING,
    COMPLETED,
    FAILED_CONVERSION,
    FAILED_REINDEX,
}
