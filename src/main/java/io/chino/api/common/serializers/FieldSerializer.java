package io.chino.api.common.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import io.chino.api.common.Field;

import java.io.IOException;

public class FieldSerializer extends StdSerializer<Field> {

    protected FieldSerializer() {
        super(Field.class);
    }

    protected FieldSerializer(Class<Field> t) {
        super(t);
    }

    @Override
    public void serialize(Field value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();

        // Always write 'name' and 'type' properties
        gen.writeFieldName("name");
        gen.writeString(value.getName());
        gen.writeFieldName("type");
        gen.writeString(value.getType());

        // 'indexed': write only if it's not 'null'
        if (value.getIndexed() != null) {
            gen.writeFieldName("indexed");
            gen.writeBoolean(value.getIndexed());
        }

        // 'insensitive': write only if it's not 'null'
        if (value.getInsensitive() != null) {
            gen.writeFieldName("insensitive");
            gen.writeBoolean(value.getInsensitive());
        }

        // 'default': write only if it's defined (it may be 'null')
        if (value.hasDefault()) {
            gen.writeFieldName("default");
            gen.writeObject(value.getDefault());
        }

        gen.writeEndObject();
    }
}