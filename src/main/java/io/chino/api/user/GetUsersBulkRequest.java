package io.chino.api.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.ArrayList;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "ids"
})
public class GetUsersBulkRequest {

    @JsonProperty("ids")
    private List<String> userIds = new ArrayList<>();


    public GetUsersBulkRequest(List<String> userIds) {
        this.userIds = userIds;
    }


    /**
     * @return user IDs
     */
    public List<String> getUserIds() {
        return userIds;
    }

    /**
     * @param userIds the user IDs
     */
    public void setUserIds(List<String> userIds) {
        this.userIds = userIds;
    }

    @Override
    public String toString() {
        return "GetUsersBulkRequest{" +
                "userIds=" + userIds +
                '}';
    }
}