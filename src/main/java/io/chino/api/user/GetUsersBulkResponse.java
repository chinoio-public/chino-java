package io.chino.api.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


import java.util.HashMap;
import java.util.Map;


/**
 * Wraps a {@link Map} of {@link User Users} returned as a response to an API call
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "total_count",
        "users",
        "errors"
})
public class GetUsersBulkResponse {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("total_count")
    private Integer totalCount;
    @JsonProperty("users")
    private Map<String, User> users = new HashMap<>();
    @JsonProperty("errors")
    private Map<String, String> errors = new HashMap<>();

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return the total count
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount The total count
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return the users
     */
    public Map<String, User> getUsers() {
        return users;
    }

    /**
     * @param users The users
     */
    public void setUsers(Map<String, User> users) {
        this.users = users;
    }

    /**
     * @return the errors
     */
    public Map<String, String> getErrors() {
        return errors;
    }

    /**
     * @param errors The errors
     */
    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "GetUsersBulkResponse{" +
                "count=" + count +
                ", totalCount=" + totalCount +
                ", users=" + users +
                ", errors=" + errors +
                '}';
    }
}
