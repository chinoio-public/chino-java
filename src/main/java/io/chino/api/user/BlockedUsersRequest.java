package io.chino.api.user;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "user_id",
    "ip",
})
public class BlockedUsersRequest {

    @JsonProperty("user_id")
    private String userId;
    @JsonProperty("ip")
    private String ipAddr;

    public BlockedUsersRequest() {
        
    }

    public BlockedUsersRequest(String userId, String ipAddr) {
        setUserId(userId);
        setIpAddr(ipAddr);
    }

    /**
     * 
     * @return the user_id
     */
    @JsonProperty("user_id")
    public String getUserId() {
        return userId;
    }

    /**
     * 
     * @param userId the user_id
     */
    @JsonProperty("user_id")
    public void setUserId(String userId) {
        this.userId = userId;
    }
    
    /**
     * 
     * @return the IP address
     */
    @JsonProperty("ip")
    public String getIpAddr() {
        return ipAddr;
    }

    /**
     * 
     * @param ipAddr the IP address
     */
    @JsonProperty("ip")
    public void setIpAddr(String ipAddr) {
        this.ipAddr = ipAddr;
    }
}
