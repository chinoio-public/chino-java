
package io.chino.api.permission;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.chino.java.Permissions;
import org.jetbrains.annotations.Nullable;

import java.util.*;

/**
 * Contains information about permissions that are active on a Chino.io resource.
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "access",
        "parent_id",
        "resource_id",
        "resource_type",
        "permission"
})
public class Permission implements PermissionsContainer {
    @JsonProperty("access")
    private String access;
    @JsonProperty("parent_id")
    private String parentId;
    @JsonProperty("resource_id")
    private String resourceId;
    @JsonProperty("resource_type")
    private String resourceType;
    @JsonProperty("permission")
    private HashMap permission;

    /**
     * Get the access level this {@link Permission} refers to.
     * May be "Structure" or "Data".
     *
     * @return either "Structure" (permission is applied on the resource's metadata)
     *      or "Data" (permission is applied on the resource's content)
     */
    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    /**
     * Get the ID on Chino.io of the parent resource of the object of this {@link Permission}
     *
     * @see #getResourceId() Get this resource's ID instead
     *
     * @return the Chino.io ID of the parent object. If the resource has no parent, returns {@code null}
     */
    @Nullable
    public String getParentId() {
        return parentId;
    }

    public void setParentId(String parentId) {
        this.parentId = parentId;
    }

    /**
     * Get the ID on Chino.io of the resource of this {@link Permission}
     *
     * @return the Chino.io ID of the object this {@link Permission} applies to.
     */

    public String getResourceId() {
        return resourceId;
    }

    public void setResourceId(String resourceId) {
        this.resourceId = resourceId;
    }

    /**
     * Get the Chino.io resource type of the object of this {@link Permission}
     *
     * @return the name of a Chino.io resource (e.g. Schema, Document, Group...) as a {@link String}
     */
    public String getResourceType() {
        return resourceType;
    }

    public void setResourceType(String resourceType) {
        this.resourceType = resourceType;
    }

    /**
     * Get the permissions that are active on this object.
     *
     *
     * @return a {@link HashMap} that may contain up to three lists of Permissions:
     *  <ul>
     *      <li>
     *          <code>"manage"</code> Permissions, i.e. permissions that are active for the user itself.
     *          Cast to a {@link java.util.List List&lt;String&gt;}.
     *      </li>
     *      <li>
     *          <code>"authorize"</code> Permissions, i.e. permissions the user can grant to third party users.
     *          Cast to a {@link java.util.List List&lt;String&gt;}.
     *      </li>
     *      <li>
     *          <code>"created_document"</code> Permissions, which defines the default permissions on children
     *          resources in Repositories, Schemas and UserSchemas. Contains a <code>"manage"</code> and a
     *          <code>"authorize"</code> list itself.
     *      </li>
     *  </ul>
     */
    public HashMap getPermission() {
        return permission;
    }

    public void setPermission(HashMap permission) {
        this.permission = permission;
    }

    /**
     * Null-safe method to read the Permissions lists. <br>
     * This method never returns {@code null} and, if the {@code permission} field is null it will be initialized to an
     * empty {@link HashMap}.
     *
     * @param name name of the list. Can be "Manage", "Authorize", "created_document.Manage" or
     *             "created_document.Authorize". Other values will cause an {@link IllegalArgumentException}.
     *
     * @return a {@link List} of Permissions.
     */
    public List<String> getPermissionsList(String name) {
        // prevent null pointer exception
        if (permission == null) {
            setPermission(new HashMap());
        }
        // validate 'name'
        HashSet<String> validNames = new HashSet<>();
        validNames.add("Manage");
        validNames.add("Authorize");
        validNames.add("created_document.Manage");
        validNames.add("created_document.Authorize");
        if (!validNames.contains(name)) {
            throw new IllegalArgumentException(String.format("Invalid Permissions list name: %s", name));
        }

        // Extract List of Permissions
        Map permsMap = getPermission();
        if (name.contains("created_document")) {
            // extract "created_document", if present.
            Object createdDocument = permsMap.getOrDefault("created_document", new HashMap<>());
            // handle different data types and convert them to a Map
            if (createdDocument == null) {
                permsMap = new HashMap();
            } else if (createdDocument instanceof Map) {
                permsMap = (Map) createdDocument;
            } else if (createdDocument instanceof Collection && ((Collection) createdDocument).isEmpty()) {
                permsMap = new HashMap();
            } else {
                String msg = String.format("Invalid class for 'created_document' (%s)", createdDocument.getClass().getName());
                throw new RuntimeException(msg);
            }
            // read the list nested inside "created_document"
            name = name.replace("created_document.", "");
        }

        Object permsList = permsMap.getOrDefault(name, new ArrayList<String>());
        // Read the list of Permissions. Handle different data types and convert them to a List<String>
        List<String> stringPerms;
        if (permsList == null) {
            stringPerms = new LinkedList<>();
        } else if (permsList instanceof List) {
            stringPerms = (List<String>) permsList;
        } else if (permsList instanceof Collection) {
            stringPerms = new LinkedList<>((Collection<? extends String>) permsList);
        } else if (permsList instanceof Map && ((Map) permsList).isEmpty()) {
            // Handles a particular case where an empty list is returned as a JSON object
            // (and thus serialized as a Map) instead of the expected list.
            stringPerms = new LinkedList<>();
        } else {
            String msg = String.format("Invalid class found in 'permissions' (%s)", permsList.getClass().getName());
            throw new RuntimeException(msg);
        }

        return stringPerms;
    }

    /**
     * Null-safe method to set the Permissions lists. <br>
     * This method ensures that {@code null} is never written in the Permissions lists.
     *
     * @param name name of the list. Can be "Manage", "Authorize", "created_document.Manage" or
     *             "created_document.Authorize". Other values will cause an {@link IllegalArgumentException}.
     * @param permissions a list of Strings representing Permissions. If null, an empty {@link List} will be
     *                    instantiated and set to the desired value.
     */
    public void setPermissionsList(String name, List<String> permissions) {
        // prevent null pointer exceptions
        if (this.permission == null) {
            setPermission(new HashMap());
        }
        if (permissions == null) {
            permissions = new LinkedList<>();
        }
        // validate 'name'
        HashSet<String> validNames = new HashSet<>();
        validNames.add("Manage");
        validNames.add("Authorize");
        validNames.add("created_document.Manage");
        validNames.add("created_document.Authorize");
        if (!validNames.contains(name)) {
            throw new IllegalArgumentException(String.format("Invalid Permissions list name: %s", name));
        }

        // update Permissions
        HashMap tmpPermissions = getPermission();
        if (name.contains("created_document")) {
            String shortName = name.replace("created_document.", "");

            Object tmpCreatedDocument = tmpPermissions.getOrDefault("created_document", new HashMap<>());
            if (tmpCreatedDocument == null) {
                tmpCreatedDocument = new HashMap<>();
            } else if (tmpCreatedDocument instanceof Collection && ((Collection) tmpCreatedDocument).isEmpty()) {
                tmpCreatedDocument = new HashMap<>();
            } else if (tmpCreatedDocument instanceof Map) {
                // no further actions needed
            } else {
                String msg = String.format("Invalid class found in 'created_document' (%s)", tmpCreatedDocument.getClass().getName());
                throw new RuntimeException(msg);
            }
            // at this point either we have cast tmpCreatedDocument to a Map, or there was an Exception
            ((Map) tmpCreatedDocument).put(shortName, new ArrayList<>(permissions));
            tmpPermissions.put("created_document", tmpCreatedDocument);
        } else {
            tmpPermissions.put(name, new ArrayList<>(permissions));
        }
        setPermission(tmpPermissions);
    }

    @Override
    public List<Permissions.Type> getManagePermissions() {
        List<String> stringPerms = this.getPermissionsList("Manage");
        // Convert Strings to Permissions.Type
        LinkedList<Permissions.Type> perms = new LinkedList<>();
        for (String s : stringPerms) {
            perms.add(Permissions.Type.fromString(s));
        }

        return perms;
    }

    @Override
    public List<Permissions.Type> getAuthorizePermissions() {
        List<String> stringPerms = this.getPermissionsList("Authorize");
        // Convert Strings to Permissions.Type
        LinkedList<Permissions.Type> perms = new LinkedList<>();
        for (String s : stringPerms) {
            perms.add(Permissions.Type.fromString(s));
        }

        return perms;
    }

    @Override
    public List<Permissions.Type> getManagePermissionsOnCreatedDocuments() {
        List<String> stringPerms = this.getPermissionsList("created_document.Manage");
        // Convert Strings to Permissions.Type
        LinkedList<Permissions.Type> perms = new LinkedList<>();
        for (String s : stringPerms) {
            perms.add(Permissions.Type.fromString(s));
        }

        return perms;
    }

    @Override
    public List<Permissions.Type> getAuthorizePermissionsOnCreatedDocuments() {
        List<String> stringPerms = this.getPermissionsList("created_document.Authorize");
        // Convert Strings to Permissions.Type
        LinkedList<Permissions.Type> perms = new LinkedList<>();
        for (String s : stringPerms) {
            perms.add(Permissions.Type.fromString(s));
        }

        return perms;
    }

    @Override
    public String toString(){
        HashMap permission = getPermission();

        String permsString;
        if (permission == null) {
            permsString = "null";
        } else {
            try {
                permsString = new ObjectMapper().writeValueAsString(permission);
            } catch (JsonProcessingException e) {
                permsString = String.format("(%s) %s", e.getClass().getSimpleName(), permission.toString());
            }
        }

        StringBuilder s = new StringBuilder("Permission:").append("\n");
        s.append("access: ").append(getAccess()).append(",\n");
        s.append("parentId: ").append(getParentId()).append(",\n");
        s.append("resourceId: ").append(getResourceId()).append(",\n");
        s.append("resourceType: ").append(getResourceType()).append(",\n");
        s.append("permission: ").append(permsString);
        s.append("\n");
        return s.toString();
    }
}
