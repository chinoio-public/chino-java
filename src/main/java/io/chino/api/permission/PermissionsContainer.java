package io.chino.api.permission;

import io.chino.java.Permissions;

import java.util.List;

/**
 * This interface is implemented by classes that hold information about Permissions on Chino.io,
 * which use the following structure:
 * <pre>
 *     {
 *         "manage" : [ permissions ... ],
 *         "authorize" : [ permissions ... ],
 *         "created_document" : {
 *              "manage" : [ permissions ... ],
 *              "authorize" : [ permissions ... ]
 *         }
 *     }
 * </pre>
 *
 * For more information about the Permission format, visit the
 * <a href="https://docs.test.chino.io/custodia/docs/v1#permissions">Permissions</a> page on the Chino.io API docs.
 *
 * @see Permissions.Type
 */
public interface PermissionsContainer {

    /**
     * Get the {@code "manage"} list of Permissions
     *
     * @return a {@link List} of {@link io.chino.java.Permissions.Type}
     */
    List<Permissions.Type> getManagePermissions();

    /**
     * Get the {@code "authorize"} list of Permissions
     *
     * @return a {@link List} of {@link io.chino.java.Permissions.Type}
     */
    List<Permissions.Type> getAuthorizePermissions();

    /**
     * Get the {@code "created_document.manage"} list of Permissions.<br>
     * <b>Note:</b> the {@code "created_document"} list is only available for requests that set Permissions
     * on children of a Schema. For any other request, implementations of this method must return {@code null}.
     *
     * @return a {@link List} of {@link io.chino.java.Permissions.Type}, or {@code null} if there is no
     *        {@code "created_document"} list
     */
    List<Permissions.Type> getManagePermissionsOnCreatedDocuments();

    /**
     * Get the {@code "created_document.authorize"} list of Permissions.<br>
     * <b>Note:</b> the {@code "created_document"} list is only available for requests that set Permissions
     * on children of a Schema. For any other request, implementations of this method must return {@code null}.
     *
     * @return a {@link List} of {@link io.chino.java.Permissions.Type}, or {@code null} if there is no
     *        {@code "created_document"} list
     */
    List<Permissions.Type> getAuthorizePermissionsOnCreatedDocuments();
}
