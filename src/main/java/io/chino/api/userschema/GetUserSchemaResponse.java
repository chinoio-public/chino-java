
package io.chino.api.userschema;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.logging.Logger;


/**
 * Wraps a {@link UserSchema} returned as a response to an API call
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "result",
    "user_schema",
    "msg"
})
public class GetUserSchemaResponse {

    @JsonProperty("result")
    private String result;
    @JsonProperty("user_schema")
    private UserSchema userSchema;
    @JsonProperty("msg")
    private String msg;

    /**
     * 
     * @return
     *     The result
     */
    @JsonProperty("result")
    public String getResult() {
        return result;
    }

    /**
     * 
     * @param result
     *     The result
     */
    @JsonProperty("result")
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * 
     * @return
     *     The UserSchema
     */
    @JsonProperty("user_schema")
	public UserSchema getUserSchema() {
		return userSchema;
	}

    /**
     * 
     * @param userSchema
     *     The UserSchema
     */
    @JsonProperty("user_schema")
	public void setUserSchema(UserSchema userSchema) {
		this.userSchema = userSchema;
	}

    /**
     * 
     * @return
     *     The msg if it's defined, otherwise returns {@code null}.
     */
    @JsonProperty("msg")
    public String getMsg() {
        return msg;
    }

    /**
     * 
     * @param msg
     *    The msg
     */
    @JsonProperty("msg")
    public void setMsg(String msg) {
        this.msg = msg;

        // If msg is not empty, log it to the application logs
        if (msg != null && !msg.equals("")) {
            String logMsg = "Received UserSchema msg from Chino API: '" + msg + "'.";
            if (this.getUserSchema() != null) {
                logMsg += " UserSchema ID: " + this.getUserSchema().getUserSchemaId();
            }
            Logger.getLogger(this.getClass().getName()).warning(logMsg);
        }
    }   

}
