package io.chino.api.document;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import java.util.HashMap;
import java.util.Map;


/**
 * Wraps a {@link Map} of {@link Document Documents} returned as a response to an API call
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
        "count",
        "total_count",
        "documents",
        "errors"
})
public class GetDocumentsBulkResponse {

    @JsonProperty("count")
    private Integer count;
    @JsonProperty("total_count")
    private Integer totalCount;
    @JsonProperty("documents")
    private Map<String, Document> documents = new HashMap<>();
    @JsonProperty("errors")
    private Map<String, String> errors = new HashMap<>();

    /**
     * @return the count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return the total count
     */
    public Integer getTotalCount() {
        return totalCount;
    }

    /**
     * @param totalCount The total count
     */
    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    /**
     * @return the documents
     */
    public Map<String, Document> getDocuments() {
        return documents;
    }

    /**
     * @param documents The documents
     */
    public void setDocuments(Map<String, Document> documents) {
        this.documents = documents;
    }

    /**
     * @return the errors
     */
    public Map<String, String> getErrors() {
        return errors;
    }

    /**
     * @param errors The errors
     */
    public void setErrors(Map<String, String> errors) {
        this.errors = errors;
    }

    @Override
    public String toString() {
        return "GetDocumentsBulkResponse{" +
                "count=" + count +
                ", totalCount=" + totalCount +
                ", documents=" + documents +
                ", errors=" + errors +
                '}';
    }
}
