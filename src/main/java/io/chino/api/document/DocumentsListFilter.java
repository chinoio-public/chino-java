package io.chino.api.document;

import io.chino.api.common.UrlFilters;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.Date;

/**
 * Filter items in a list of Documents.<br>
 * <br>
 *
 * <h2>Usage</h2>
 * Create an instance either with {@link #filterBy()} (recommended) or {@link #DocumentsListFilter()}.<br>
 * <br>
 * Chain filters like this:
 * <pre>
 * DocumentsListFilter filter = DocumentsListFilter.filterBy()
 *         .insertDate_greaterThan(date)
 *         .lastUpdate_lowerThan(date)
 *         .isActive(true);
 * </pre>
 * If 2 or more filters are specified, they will be applied with an "AND" operation e.g.:
 * <pre>
 * is_active == true AND insert_date &gt; 12-05-2021T00:00:00
 * </pre>
 *
 * <h2>Available filters</h2>
 * <ul>
 *     <li>{@link #isActive(Boolean)}: filter by value of {@code is_active}</li>
 *     <li>{@link #insertDate_greaterThan(Date)} filter documents created after this date</li>
 *     <li>{@link #insertDate_lowerThan(Date)} filter documents created before this date</li>
 *     <li>{@link #lastUpdate_greaterThan(Date)} filter documents updated after this date</li>
 *     <li>{@link #lastUpdate_lowerThan(Date)} filter documents updated before this date</li>
 * </ul>
 *
 * Dates must be {@link String Strings} in format {@code YYYY-MM-DDTHH:MM:SS}, {@link Date} objects or {@link LocalDateTime} objects.
 * <b>All dates are considered as UTC.</b>
 *
 */
public class DocumentsListFilter extends UrlFilters {
    private final DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss");

    /**
     * Create a new {@link DocumentsListFilter}
     */
    private DocumentsListFilter() {}

    /**
     * Create a new {@link DocumentsListFilter}
     */
    public static DocumentsListFilter filterBy() {
        return new DocumentsListFilter();
    }


    /* is_active */

    /**
     * Filter by value of {@code is_active}
     *
     * @param isActive {@code true} or {@code false}
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter isActive(Boolean isActive) {
        String value = (isActive) ? "true" : "false";
        super.addFilter("is_active", value);

        return this;
    }


    /* insert_date */

    /**
     * Filter documents created after the provided date
     *
     * @param insertDate the creation date
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter insertDate_greaterThan(LocalDateTime insertDate) {
        String value = insertDate.format(dateTimeFormatter);
        super.addFilter("insert_date__gt", value);

        return this;
    }

    /**
     * Filter documents created after the provided date
     *
     * @param insertDate the creation date
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter insertDate_greaterThan(Date insertDate) {
        LocalDateTime ldt = insertDate.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
        return this.insertDate_greaterThan(ldt);
    }

    /**
     * Filter documents created after the provided date
     *
     * @param insertDate the creation date
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter insertDate_greaterThan(String insertDate) throws Exception {
        try {
            LocalDateTime ldt = LocalDateTime.from(dateTimeFormatter.parse(insertDate));
            return this.insertDate_greaterThan(ldt);
        } catch (DateTimeParseException e) {
            throw new Exception("Invalid datetime format. Please use YYYY-MM-DDTHH:MM:SS");
        }
    }


    /**
     * Filter documents created before the provided date
     *
     * @param insertDate the creation date
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter insertDate_lowerThan(LocalDateTime insertDate) {
        String value = insertDate.format(dateTimeFormatter);
        super.addFilter("insert_date__lt", value);

        return this;
    }

    /**
     * Filter documents created before the provided date
     *
     * @param insertDate the creation date
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter insertDate_lowerThan(Date insertDate) {
        LocalDateTime ldt = insertDate.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
        return this.insertDate_lowerThan(ldt);

    }

    /**
     * Filter documents created before the provided date
     *
     * @param insertDate the creation date
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter insertDate_lowerThan(String insertDate) throws Exception {
        try {
            LocalDateTime ldt = LocalDateTime.from(dateTimeFormatter.parse(insertDate));
            return this.insertDate_lowerThan(ldt);
        } catch (DateTimeParseException e) {
            throw new Exception("Invalid datetime format. Please use YYYY-MM-DDTHH:MM:SS");
        }
    }


    /* last_update */

    /**
     * Filter documents last updated after the provided date
     *
     * @param lastUpdate the date of the last update
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter lastUpdate_greaterThan(LocalDateTime lastUpdate) {
        String value = lastUpdate.format(dateTimeFormatter);
        super.addFilter("last_update__gt", value);

        return this;
    }

    /**
     * Filter documents last updated after the provided date
     *
     * @param lastUpdate the date of the last update
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter lastUpdate_greaterThan(Date lastUpdate) {
        LocalDateTime ldt = lastUpdate.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
        return this.lastUpdate_greaterThan(ldt);

    }

    /**
     * Filter documents last updated after the provided date
     *
     * @param lastUpdate the date of the last update
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter lastUpdate_greaterThan(String lastUpdate) throws Exception {
        try {
            LocalDateTime ldt = LocalDateTime.from(dateTimeFormatter.parse(lastUpdate));
            return this.lastUpdate_greaterThan(ldt);
        } catch (DateTimeParseException e) {
            throw new Exception("Invalid datetime format. Please use YYYY-MM-DDTHH:MM:SS");
        }
    }


    /**
     * Filter documents last updated before the provided date
     *
     * @param lastUpdate the date of the last update
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter lastUpdate_lowerThan(LocalDateTime lastUpdate) {
        String value = lastUpdate.format(dateTimeFormatter);
        super.addFilter("last_update__lt", value);

        return this;
    }

    /**
     * Filter documents last updated before the provided date
     *
     * @param lastUpdate the date of the last update
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter lastUpdate_lowerThan(Date lastUpdate) {
        LocalDateTime ldt = lastUpdate.toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime();
        return this.lastUpdate_lowerThan(ldt);

    }

    /**
     * Filter documents last updated before the provided date
     *
     * @param lastUpdate the date of the last update
     *
     * @return this instance of {@link DocumentsListFilter} for chained calls.
     */
    public DocumentsListFilter lastUpdate_lowerThan(String lastUpdate) throws Exception {
        try {
            LocalDateTime ldt = LocalDateTime.from(dateTimeFormatter.parse(lastUpdate));
            return this.lastUpdate_lowerThan(ldt);
        } catch (DateTimeParseException e) {
            throw new Exception("Invalid datetime format. Please use YYYY-MM-DDTHH:MM:SS");
        }
    }
}
