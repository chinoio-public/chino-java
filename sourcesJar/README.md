#  CHINO.io Java SDK

Official Java wrapper for [**Chino.io**](https://www.chino.io) API
 
See [Full API docs](https://docs.test.chino.io/custodia/docs/v1)
 
## Sources
We do not provide sources JAR with this release.

If interested, please drop us an email at
[tech-support@chino.io](mailto:tech-support@chino.io).

Feel free to contact us for any clarification regarding our service and
the SDK.